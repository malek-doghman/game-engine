#include "BoundingBox.h"
#include "ShaderProgram.h"
#include "Camera.h"
#include "WindowGL.h"
#include "ShadowFrameBuffer.h"
#include "Utility.h"


VIBuffer BoundingBox::bufferSpecs;
ShaderProgram BoundingBox::boundBox;
float BoundingBox::closestDepth = -1.0f;

BoundingBox::BoundingBox(){
	closeDepth = -1.0f;
	initialized = selected = false;

}

void BoundingBox::Initialize(){
	if (!bufferInitilaized()){
		InitializeBuffers();
	}
}

void BoundingBox::updateBounds(vec3 _v){
	if (!initialized){
		max = min = _v;
		initialized = true;
	}

	else{
		if (_v.x < min.x){
			min.x = _v.x;
		}
		if (_v.y < min.y){
			min.y = _v.y;
		}

		if (_v.z < min.z){
			min.z = _v.z;
		}

		if (_v.x > max.x){
			max.x = _v.x;
		}

		if (_v.y > max.y){
			max.y = _v.y;
		}

		if (_v.z > max.z){
			max.z = _v.z;
		}

	}

}


bool BoundingBox::bufferInitilaized(){
	return(bufferSpecs.indexCount > 0);
}

void BoundingBox::InitializeBuffers(){

	bufferSpecs = Utility::getCube();

	boundBox.InsertShader(GL_VERTEX_SHADER, "data/shader/boundBox.vert");
	boundBox.InsertShader(GL_FRAGMENT_SHADER, "data/shader/boundBox.frag");
	boundBox.BuildProgram();


}

void BoundingBox::Update(const mat4& _WorldMatrix){
	mat4 scale = glm::scale(mat4(), max - min);
	vec3 t = max + min;
	mat4 trans = glm::translate(mat4(), vec3(t.x / 2, t.y / 2, t.z / 2));
	worldMatrix = trans * _WorldMatrix *  scale;
	InitializePlanes();
	transformPlanes();
}

void BoundingBox::Draw(){
	if (!ShadowFrameBuffer::isShadowPass()){

		/*Note : Blending will mess up when a bounding box is drawn with blending
		This has happened since I added multi-threading.
		Check and Correct blending for the whole application !!!!!
		/*


		glEnable(GL_LINE_SMOOTH);
		Utility::enableBlending(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);

		*/

		glBindVertexArray(bufferSpecs.VAO);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, bufferSpecs.indexBuffer);
		glUseProgram(boundBox.getProgram());
		boundBox.sendUniformData("WVPMatrix", Camera::getViewProjectionMatrix()*worldMatrix);
		glDrawElements(GL_LINES, bufferSpecs.indexCount, GL_UNSIGNED_INT, 0);
		glBindVertexArray(0);
		glUseProgram(0);


		/*
		glDisable(GL_LINE_SMOOTH);
		Utility::resetLastBlendingState();
		*/

	}
}

void BoundingBox::InitializePlanes(){
	pairedPlanes.clear();
	Plane bottom(vec3(-0.5, -0.5, 0.5), vec3(-0.5, -0.5, -0.5), vec3(0.5, -0.5, 0.5));
	Plane top(vec3(-0.5, 0.5, -0.5), vec3(-0.5, 0.5, 0.5), vec3(0.5, 0.5, 0.5));

	Plane front(vec3(-0.5, -0.5, 0.5), vec3(0.5, -0.5, 0.5), vec3(-0.5, 0.5, 0.5));
	Plane back(vec3(-0.5, 0.5, -0.5), vec3(0.5, 0.5, -0.5), vec3(0.5, -0.5, -0.5));

	Plane right(vec3(0.5, 0.5, -0.5), vec3(0.5, 0.5, 0.5), vec3(0.5, -0.5, 0.5));
	Plane left(vec3(-0.5, 0.5, 0.5), vec3(-0.5, 0.5, -0.5), vec3(-0.5, -0.5, -0.5));

	pairedPlanes.push_back(pair<Plane, Plane>(bottom, top));
	pairedPlanes.push_back(pair<Plane, Plane>(front, back));
	pairedPlanes.push_back(pair<Plane, Plane>(right, left));
}

bool BoundingBox::isIntersected(){

	if (glfwGetMouseButton(WindowGL::GetWindowPointer(), GLFW_MOUSE_BUTTON_1)){
		//unselect all
		selected = false;

		vec3 I11, I12;
		vec3 I21, I22;
		vec3 R0 = Camera::getEyePos();
		vec3 Rd = Camera::getRayDirection();
		for (unsigned int i = 0; i < pairedPlanes.size(); i++){
			for (unsigned int j = i + 1; j < pairedPlanes.size(); j++){
				if (pairIntersected(pairedPlanes[j], I21, I22, Rd, R0) && pairIntersected(pairedPlanes[i], I11, I12, Rd, R0)){
					if (((intersectionWithRectangle(pairedPlanes[i], I11, I12))) || (intersectionWithRectangle(pairedPlanes[j], I21, I22))){
						float dP11 = glm::length(I11 - R0);
						float dP12 = glm::length(I12 - R0);

						float dP21 = glm::length(I21 - R0);
						float dP22 = glm::length(I22 - R0);

						float minP1 = std::min(dP11, dP12);
						float maxP1 = std::max(dP11, dP12);

						float minP2 = std::min(dP21, dP22);
						float maxP2 = std::max(dP21, dP22);

						closeDepth = std::min(minP1, minP2);


						if (std::max(minP1, minP2) < std::min(maxP1, maxP2)){

							if ((closestDepth > closeDepth) || (closestDepth == -1)){
								closestDepth = closeDepth;
								selected = true;
								return(true);
							}

						}

					}
				}
			}
		}
	}
}

bool BoundingBox::pairIntersected(const pair<Plane, Plane>& p, vec3& I1, vec3& I2, const vec3& Rd, const vec3& R0){
	return(p.first.isRayIntersected(I1, Rd, R0) && p.second.isRayIntersected(I2, Rd, R0));
}

bool BoundingBox::isSelected(){
	if (closeDepth != closestDepth){
		selected = false;
	}
	return(selected);
}

void BoundingBox::transformPlanes(){
	for (pair<Plane, Plane>& p : pairedPlanes){
		p.first.transformMat4x4(worldMatrix);
		p.second.transformMat4x4(worldMatrix);
	}
}

bool BoundingBox::isInsideBox(const vec3& pt){
	return ((pt.x < max.x) && (pt.y<max.y) && (pt.z<max.z) &&
		(pt.x>min.x) && (pt.y>min.y) && (pt.z > min.z));
}

void BoundingBox::InitilaizeClosest(){
	if (glfwGetMouseButton(WindowGL::GetWindowPointer(), GLFW_MOUSE_BUTTON_1)){
		closestDepth = -1.0f;
	}
}


bool BoundingBox::intersectionWithRectangle(const pair<Plane, Plane>& p, vec3& I1, vec3& I2){
	return(p.first.inRectangle(I1) || p.first.inRectangle(I2) || p.second.inRectangle(I1) || p.second.inRectangle(I2));
}

const pair<vec3, vec3> BoundingBox::getBounds() const{
	return(pair<vec3, vec3>(min, max));
}