#pragma once
#include "Common.h"
#include "ShaderProgram.h"
#include "Plane.h"
#include "WindowGL.h"

class BoundingBox{
public:
	BoundingBox();
	void updateBounds(vec3 v);
	void Initialize();
	void Draw();
	void Update(const mat4& worldMatrix = mat4());
	bool isIntersected();
	bool isSelected();
	const pair<vec3, vec3> getBounds() const;
	static void InitilaizeClosest();
private:
	static void InitializeBuffers();
	static bool bufferInitilaized();
	void InitializePlanes();
	bool isInsideBox(const vec3& pt);
	bool intersectionWithRectangle(const pair<Plane, Plane>& p, vec3& I1, vec3& I2);
	void transformPlanes();
	bool pairIntersected(const pair<Plane, Plane>& p, vec3& I1, vec3& I2, const vec3& Rd, const vec3& R0);
	vec3 min;
	vec3 max;
	mat4 worldMatrix;
	static VIBuffer bufferSpecs;
	static ShaderProgram boundBox;
	bool initialized;
	bool selected;
	vector<pair<Plane, Plane>>pairedPlanes;
	float closeDepth;
	static float closestDepth;
};