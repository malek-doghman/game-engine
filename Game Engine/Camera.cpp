#include "Camera.h"
#include "WindowGL.h"
#include "Light.h"
#include "Terrain.h"


vec3 Camera::cPosition = vec3(0.0);
vec3 Camera::cDirection = vec3(0.0);
vec3 Camera::wRay = vec3(0.0);
mat4 Camera::perspectiveMatrix = mat4();
mat4 Camera::viewMatrix = mat4();
mat4 Camera::WVPMatrix = mat4();
mat4 Camera::WVPCopyMatrix = mat4();
mat4 Camera::oldWVPMatrix = mat4();
bool Camera::rotate = false;

/******************************************************
		Initializing Camera Directions/Position
******************************************************/

Camera::Camera(WindowGL& window_, Terrain* t) :GameComponent(true, false), window(&window_), terrain(t)
{
	cUp = vec3(0.0, 1.0, 0.0);
	cRight = vec3(1.0, 0.0, 0.0);
	cDirection = vec3(0.0, 0.0, -1.0);
	if (t != nullptr)
		cPosition = vec3(t->getScale() / 2, 130.0, -t->getScale() / 2);
	lightSwitch = false;

}

Camera::Camera(WindowGL& window_, Terrain* t, float x, float y, float z) :GameComponent(true, false), window(&window_), terrain(t)
{
	cUp = vec3(0.0, 1.0, 0.0);
	cRight = vec3(1.0, 0.0, 0.0);
	cDirection = vec3(0.0, 0.0, -1.0);
	cPosition = glm::vec3(x, y, z);
	lightSwitch = false;
}




/******************************************************
		Initializing Camera's Perspective Matrix
******************************************************/
void Camera::InitializePersepctiveMatrix(){
	perspectiveMatrix = glm::perspective(40.0f, window->GetAspectRatio(), 0.1f, 1000000.0f);
}




/******************************************************
		Initializing Camera's View Matrix
*******************************************************/
void Camera::UpdateViewMatrix(){
	oldWVPMatrix = WVPMatrix;

	if (terrain != nullptr)
		cPosition.y = 0.90 * cPosition.y + 0.10 * (terrain->getHeightAt(vec3(cPosition.x, 0.0, cPosition.z)) + 130);
	glm::vec3 target = cPosition + cDirection;
	viewMatrix = glm::lookAt(cPosition, target, cUp);
	WVPMatrix = perspectiveMatrix * viewMatrix;
}

void Camera::Initialize(){
	InitializePersepctiveMatrix();
	UpdateViewMatrix();
	InitializeMousePosition();

}

void Camera::Update(){
	CameraControl();
	UpdateViewMatrix();

}

glm::mat4 Camera::getViewProjectionMatrix(){
	return(WVPMatrix);
}

void Camera::InitializeMousePosition(){
	glfwGetCursorPos(window->GetWindowPointer(), &mouseX, &mouseY);

}


/******************************************************
	This Method is called inside the Update() method
*******************************************************/
void Camera::CameraControl(){
	static double viewCenterX = WindowGL::GetWidth() / 2;
	static double viewCenterY = WindowGL::GetHeight() / 2;
	static vec2 sRot = vec2(0);

	float elapsedTime = static_cast<float>(WindowGL::getElapsedTime());
	float movAmount = 99.0f * elapsedTime;
	float rotAmount = 60.0f * elapsedTime;
	static float lastElapsed = 0;
	rotate = false;

	if (glfwGetMouseButton(window->GetWindowPointer(), GLFW_MOUSE_BUTTON_1)){

		findRay(mouseX, mouseY);

	}


	glm::mat4 rotateMatrix;

	if (glfwGetKey(window->GetWindowPointer(), GLFW_KEY_Z)){
		cPosition = cPosition + cDirection * glm::vec3(movAmount);
	}
	if (glfwGetKey(window->GetWindowPointer(), GLFW_KEY_S)){
		cPosition = cPosition - cDirection * glm::vec3(movAmount);
	}
	if (glfwGetKey(window->GetWindowPointer(), GLFW_KEY_Q)){
		cPosition = cPosition - cRight * glm::vec3(movAmount);
	}
	if (glfwGetKey(window->GetWindowPointer(), GLFW_KEY_D)){
		cPosition = cPosition + cRight * glm::vec3(movAmount);
	}

	double xPos, yPos;
	glfwGetCursorPos(window->GetWindowPointer(), &xPos, &yPos);
	

	glfwSetCursorPos(WindowGL::GetWindowPointer(), viewCenterX, viewCenterY);

	glm::vec2 rotationAmount = vec2(abs(xPos - mouseX), abs(yPos - mouseY)) * rotAmount;
	sRot = sRot*vec2(lastElapsed) + vec2(elapsedTime)*rotationAmount;
	if ((xPos > mouseX)){
		rotate = true;
		rotateMatrix = glm::rotate(rotateMatrix, -sRot.x, glm::vec3(0.0, 1.0, 0.0));
	}
	if ((xPos < mouseX)){
		rotate = true;
		rotateMatrix = glm::rotate(rotateMatrix, sRot.x, glm::vec3(0.0, 1.0, 0.0));
	}

	if ((yPos < mouseY)){
		rotate = true;
		rotateMatrix = glm::rotate(rotateMatrix, +sRot.y, cRight);
	}
	if ((yPos > mouseY)){
		rotate = true;
		rotateMatrix = glm::rotate(rotateMatrix, -sRot.y, cRight);
	}

	if (rotate == true){
		cUp = vec3(glm::normalize(rotateMatrix * vec4(cUp, 0.0)));
		cDirection = vec3(glm::normalize(rotateMatrix * vec4(cDirection, 0.0)));
		cRight = vec3(glm::normalize(glm::cross(cDirection, cUp)));
		cUp = vec3(glm::normalize(glm::cross(cRight, cDirection)));
	}


	lastElapsed = elapsedTime;
	mouseX = viewCenterX;
	mouseY = viewCenterY;

}

void Camera::Draw(ShaderProgram& shader, Light* l){

}

glm::vec3 Camera::getEyePos(){
	return(cPosition);
}

glm::vec3 Camera::getDirection(){
	return(cDirection);
}

glm::vec3 Camera::getForward(){
	return(cDirection);
}

/******************************************************
setWVPMatrix():
replaces camera's WVP matrix with the light's WVP Matrix
A copy of latest Camera's WVP matrix is
copied in WVPCopyMatrix
*******************************************************/
void Camera::setWVPMatrix(Light* l){
	if (!lightSwitch){
		WVPCopyMatrix = WVPMatrix;
		WVPMatrix = l->getVPMatrix();
		lightSwitch = true;
	}
	else{
		WVPMatrix = l->getVPMatrix();
	}
}

/******************************************************
resetLatestCameraWVP():
restores original Camera's WVP Matrix
*******************************************************/
void Camera::resetLatestCameraWVP(){
	WVPMatrix = WVPCopyMatrix;
	lightSwitch = false;
}

glm::mat4 Camera::getViewMatrix(){
	return(viewMatrix);
}

glm::mat4 Camera::getProjectionMatrix(){
	return(perspectiveMatrix);
}

glm::mat4 Camera::getOldWVPMatrix(){
	return(oldWVPMatrix);
}

glm::mat4 Camera::getInvertedViewProjectionMatrix(){
	return(glm::inverse(WVPMatrix));
}

void Camera::findRay(double cX, double cY){
	double ccX = ((cX / WindowGL::GetWidth()) * 2) - 1;
	double ccY = -((cY / WindowGL::GetHeight()) * 2) + 1;
	vec3 pos = vec3(ccX, ccY, 0.0);
	vec4 v = Camera::getInvertedViewProjectionMatrix() * vec4(pos, 1.0);
	wRay = vec3(v / v.w) - cPosition;
}

vec3 Camera::getRayDirection(){
	return(wRay);
}

int Camera::isRotated(){
	if (rotate)
		return(1);
	else{
		return(0);
	}
}
