
/*******************************************************************************************************
CAMERA CLASS
ITS BASICALLY A STATIC CLASS SO THERE CAN'T BE MORE	THAN ONE CAMERA IN THE WHOLE PROJECT
A CAMERA IS DEFINED BY ITS POSITION AND ITS DIRECTIONS
THIS CLASS CONTAINS GENERAL METHODS FOR CONSTRUCTING VIEW/PERSPECTIVE MATRICES
CAMERA CLASS IS ALSO USED TO VISUALIZE WHAT A GIVEN LIGHT IS SEEING
*******************************************************************************************************/


#pragma once
#include "Common.h"
#include "GameComponent.h"
#include "InputController.h"


class ShaderProgram;
class Light;
class WindowGL;
class Terrain;

class Camera :public GameComponent{
public:
	Camera(WindowGL& window, Terrain* t);
	Camera(WindowGL& window, Terrain* t, float x, float y, float z);
	virtual void Update();
	virtual void Draw(ShaderProgram& shader, Light* l = nullptr);
	virtual void Initialize();
	static glm::mat4 getViewProjectionMatrix();
	static glm::mat4 getInvertedViewProjectionMatrix();
	static glm::mat4 getViewMatrix();
	static glm::mat4 getProjectionMatrix();
	static glm::mat4 getOldWVPMatrix();
	static glm::vec3 getEyePos();
	static glm::vec3 getDirection();
	static vec3 getRayDirection();

	glm::vec3 getForward();
	void setWVPMatrix(Light* light);
	void resetLatestCameraWVP();

	void findRay(double cX, double cY);
	static int isRotated();

private:
	static glm::vec3 cPosition, cDirection;
	static glm::vec3 wRay;
	glm::vec3 cUp, cRight;
	static glm::mat4 viewMatrix;
	static glm::mat4 perspectiveMatrix;
	WindowGL* window;
	double mouseX, mouseY;

	static glm::mat4 WVPMatrix;
	static glm::mat4 WVPCopyMatrix;
	static glm::mat4 oldWVPMatrix;

	bool lightSwitch;

	Terrain* terrain;
	static bool rotate;


private:
	void InitializePersepctiveMatrix();
	void InitializeMousePosition();
	void UpdateViewMatrix();
	void CameraControl();
};