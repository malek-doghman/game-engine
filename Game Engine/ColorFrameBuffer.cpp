#include "ColorFrameBuffer.h"


ColorFrameBuffer::ColorFrameBuffer(int _Width, int _Height, const char* fShader, const char* vShader)
	:FrameBuffer(_Width, _Height, fShader, vShader)
{
	InitializeFrameBuffer();
}


/******************************************************
ColorFrambuffer has only a RGBA32F color texture
that is attached to its GL_COLOR_ATTACHMENT0
this framebuffer is sometimes used to store depths
in color channels.
*******************************************************/
void ColorFrameBuffer::InitializeFrameBuffer(){
	glGenFramebuffers(1, &frameBuffer);
	bindFrameBuffer();

	glGenTextures(1, &colorTexture);
	glBindTexture(GL_TEXTURE_2D, colorTexture);
	glTexStorage2D(GL_TEXTURE_2D, 1, GL_RGB32F, width, height);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);


	glGenTextures(1, &depthTexture);
	glBindTexture(GL_TEXTURE_2D, depthTexture);
	glTexStorage2D(GL_TEXTURE_2D, 1, GL_DEPTH_COMPONENT16, width, height);


	glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, colorTexture, 0);
	glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, depthTexture, 0);

	GLuint buffers[] = { GL_COLOR_ATTACHMENT0,
		GL_DEPTH_ATTACHMENT

	};

	glDrawBuffers(2, buffers);

	unBindFrameBuffer();
}