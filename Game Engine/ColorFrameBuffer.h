
/*******************************************************************************************************
COLOR_FRAMEBUFFER CLASS
THIS CLASS IS AN EXTENSION TO THE FRAMEBUFFER CLASS.
THE INTENTION OF MAKING THIS CLASS IS TO MAKE A FRAMEBUFFER THAT ONLY WRITES ON THE COLOR BUFFER
SO THERE'S NO DEPTH TESTING !!!!
IT WILL BE GENERALLY USED FOR ONLY SIMPLE FILTERS(BLUR,GAMMA CORRECTION, ...)
*******************************************************************************************************/

#pragma once
#include "Common.h"
#include "FrameBuffer.h"


class ColorFrameBuffer : public FrameBuffer{
public:
	ColorFrameBuffer(int _Width, int _Height, const char* fShader = "data/shader/screen.frag", const char* vShader = "data/shader/screen.vert");

protected:
	void InitializeFrameBuffer() override;

};