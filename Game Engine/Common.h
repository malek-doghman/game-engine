#pragma once
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <memory>
#include <vector>
#include <map>
#include<thread>
#include<unordered_map>
#include <set>
#include <list>
#include<ctime>
#include <algorithm>
#include <stdexcept>
#include <GL/glew.h>
#include "GLFW/glfw3.h"
#include "glm/glm.hpp"
#include "glm/gtx/simd_mat4.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "glm/gtc/matrix_inverse.hpp"
#include "glm/gtc/type_ptr.hpp"
#include "glm/core/func_trigonometric.hpp"
#include <SOIL.h>



#define GLFW_EXPOSE_NATIVE_WIN32
#define GLFW_EXPOSE_NATIVE_WGL

using std::string;
using std::vector;
using std::map;
using std::multimap;
using std::set;
using std::unordered_map;
using std::list;
using std::fstream;
using std::ios;
using std::pair;
using std::hash;
using std::thread;

using glm::vec3;
using glm::vec4;
using glm::vec2;
using glm::mat4;
using glm::mat3;



/******************************************************
An enumeration for supported texture types used here
*******************************************************/
enum TextureType{
	DIFFUSE_TEXTURE,
	SPECULAR_TEXTURE,
	ALPHA_TEXTURE,
	NORMALS_TEXTURE
};


/******************************************************
Texture struct which holds:
-the id of the texture returned by glGenTextures
-the TextureType
*******************************************************/
struct Texture{
	GLuint id;
	TextureType type;
	string name;
	Texture(){
		id = 0;
		width = height = mapChannels = 0;
		type = DIFFUSE_TEXTURE;
		rawData = nullptr;
	}
	GLint width, height, mapChannels;
	unsigned char* rawData;
};