#include "CompleteFrameBuffer.h"


CompleteFrameBuffer::CompleteFrameBuffer(int _Width, int _Height, const char* fShader, const char* vShader)
	:FrameBuffer(_Width, _Height, fShader, vShader)
{
	InitializeFrameBuffer();
}


/******************************************************
CompleteFrameBuffer has two texture attachements:
-Color Texture(RGBA32F)	sometimes used to store depth
-Depth(32F)/Stencil(8) Texture
*******************************************************/
void CompleteFrameBuffer::InitializeFrameBuffer(){
	glGenFramebuffers(1, &frameBuffer);
	bindFrameBuffer();

	glGenTextures(1, &colorTexture);
	glBindTexture(GL_TEXTURE_2D, colorTexture);
	glTexStorage2D(GL_TEXTURE_2D, 1, GL_RGB32F, width, height);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

	glGenTextures(1, &depthTexture);
	glBindTexture(GL_TEXTURE_2D, depthTexture);
	glTexStorage2D(GL_TEXTURE_2D, 1, GL_DEPTH_COMPONENT32F, width, height);

	glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, colorTexture, 0);
	glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, depthTexture, 0);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

	//glGenRenderbuffers(1, &rDepthStencilRenderBuffer);
	//glBindRenderbuffer(GL_RENDERBUFFER, rDepthStencilRenderBuffer);

	/****CHECK*/

	GLuint buffers[] = { GL_COLOR_ATTACHMENT0,
		GL_DEPTH_ATTACHMENT
	};

	glDrawBuffers(2, buffers);

	unBindFrameBuffer();
}