
/*******************************************************************************************************
COMPLETE_FRAMEBUFFER CLASS
THIS CLASS IS AN EXTENSION TO THE FRAMEBUFFER CLASS.
THIS IS A COMPLETE FRAMEBUFFER THAT HOLDS ALL KINDS OF BUFFERS(COLOR,DEPTH,STENCIL)
USEFUL FOR OFFLINE RENDERING
*******************************************************************************************************/


#pragma once
#include "Common.h"
#include "FrameBuffer.h"

class CompleteFrameBuffer : public FrameBuffer{
public:
	CompleteFrameBuffer(int _Width, int _Height, const char* fShader = "data/shader/screen.frag", const char* vShader = "data/shader/screen.vert");
protected:
	virtual void InitializeFrameBuffer() override;


};