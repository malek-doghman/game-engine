#include "CubeMap.h"
#include "Utility.h"
#include "ShaderProgram.h"
#include "Camera.h"

CubeMap::CubeMap() : GameComponent(true, true){
	string path("data/skybox/");
	string extension(".jpg");
	string right(path + string("right") + extension);
	string left(path + string("left") + extension);
	string top(path + string("top") + extension);
	string bottom(path + string("bottom") + extension);
	string back(path + string("back") + extension);
	string front(path + string("front") + extension);

	faces.push_back(&(right)[0]);
	faces.push_back(&(left)[0]);
	faces.push_back(&(top)[0]);
	faces.push_back(&(bottom)[0]);
	faces.push_back(&(back)[0]);
	faces.push_back(&(front)[0]);
}

CubeMap::~CubeMap()
{
}



void CubeMap::Initialize(){
	loadCubeMap();
	GLfloat skyboxVertices[] = {
		// Positions          
		-1.0f, 1.0f, -1.0f,
		-1.0f, -1.0f, -1.0f,
		1.0f, -1.0f, -1.0f,
		1.0f, -1.0f, -1.0f,
		1.0f, 1.0f, -1.0f,
		-1.0f, 1.0f, -1.0f,

		-1.0f, -1.0f, 1.0f,
		-1.0f, -1.0f, -1.0f,
		-1.0f, 1.0f, -1.0f,
		-1.0f, 1.0f, -1.0f,
		-1.0f, 1.0f, 1.0f,
		-1.0f, -1.0f, 1.0f,

		1.0f, -1.0f, -1.0f,
		1.0f, -1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, -1.0f,
		1.0f, -1.0f, -1.0f,

		-1.0f, -1.0f, 1.0f,
		-1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, -1.0f, 1.0f,
		-1.0f, -1.0f, 1.0f,

		-1.0f, 1.0f, -1.0f,
		1.0f, 1.0f, -1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		-1.0f, 1.0f, 1.0f,
		-1.0f, 1.0f, -1.0f,

		-1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f, 1.0f,
		1.0f, -1.0f, -1.0f,
		1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f, 1.0f,
		1.0f, -1.0f, 1.0f
	};

	GLuint VertexBufferObject;

	glGenVertexArrays(1, &VertexArrayObject);
	glGenBuffers(1, &VertexBufferObject);

	glBindVertexArray(VertexArrayObject);

	glBindBuffer(GL_ARRAY_BUFFER, VertexBufferObject);
	glBufferData(GL_ARRAY_BUFFER, sizeof(skyboxVertices), skyboxVertices, GL_STATIC_DRAW);

	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), (GLvoid*)0);
	glEnableVertexAttribArray(0);

	glBindVertexArray(0);

	shader.InsertShader(GL_VERTEX_SHADER, "data/shader/CubeMapShader.vert");
	shader.InsertShader(GL_FRAGMENT_SHADER, "data/shader/CubeMapShader.frag");
	shader.BuildProgram();
	shaderProgram = shader.getProgram();
}

void CubeMap::loadCubeMap(){
	glGenTextures(1, &cubeMapTexture);
	glActiveTexture(GL_TEXTURE0);

	glBindTexture(GL_TEXTURE_CUBE_MAP, cubeMapTexture);
	for (int i = 0; i < faces.size(); i++)
	{
		int h, w;
		unsigned char* image = SOIL_load_image(faces[i], &w, &h, 0, SOIL_LOAD_RGBA);
		glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_RGBA, w, h, 0, GL_RGBA, GL_UNSIGNED_BYTE, image);
		SOIL_free_image_data(image);
	}

	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
	glBindTexture(GL_TEXTURE_CUBE_MAP, 0);
}

GLuint CubeMap::getCubeMapID(){
	return cubeMapTexture;
}

void CubeMap::Update(){

}

void CubeMap::Draw(ShaderProgram& shaderP, Light* l){
	glDepthFunc(GL_LEQUAL);

	glUseProgram(shaderProgram);

	shader.sendUniformData("view", Camera::getViewMatrix());
	shader.sendUniformData("projection", Camera::getProjectionMatrix());

	glBindVertexArray(VertexArrayObject);
	glActiveTexture(GL_TEXTURE0);
	glUniform1i(glGetUniformLocation(shaderProgram, "skybox"), 0);
	glBindTexture(GL_TEXTURE_CUBE_MAP, cubeMapTexture);
	glDrawArrays(GL_TRIANGLES, 0, 36);
	glBindVertexArray(0);

	glDepthFunc(GL_LESS);
}