#pragma once

#include "Common.h"
#include "ShaderProgram.h"
#include "GameComponent.h"

class CubeMap : public GameComponent
{
public:
	CubeMap();
	~CubeMap();
	GLuint getCubeMapID();
	virtual void Initialize() override;
	virtual void Update() override;
	virtual void Draw(ShaderProgram& shader, Light* light = nullptr) override;
private:
	GLuint shaderProgram;
	void loadCubeMap();
	vector<const GLchar*> faces;
	GLuint cubeMapTexture;
	GLuint VertexArrayObject;
	ShaderProgram shader;
};