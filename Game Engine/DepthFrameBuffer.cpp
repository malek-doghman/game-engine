#include "DepthFrameBuffer.h"


DepthFrameBuffer::DepthFrameBuffer(int _Width, int _Height, const char* fShader, const char* vShader)
	:FrameBuffer(_Width, _Height, fShader, vShader)
{
	InitializeFrameBuffer();
}

/**********************************************************
DepthFrameBuffer has only a depth texture
that is attached to its GL_DEPTH_ATTACHMENT
This framebuffer has only one channel for depth values
depth values are not easily altered(only from vertex shader)
***********************************************************/

void DepthFrameBuffer::InitializeFrameBuffer(){
	glGenFramebuffers(1, &frameBuffer);
	bindFrameBuffer();

	glGenTextures(1, &depthTexture);
	glBindTexture(GL_TEXTURE_2D, depthTexture);
	glTexStorage2D(GL_TEXTURE_2D, 1, GL_DEPTH_COMPONENT32F, width, height);

	glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, depthTexture, 0);

	GLuint buffers[] = { GL_DEPTH_ATTACHMENT
	};

	glDrawBuffers(1, buffers);

	unBindFrameBuffer();
}