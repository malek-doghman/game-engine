
/*******************************************************************************************************
DEPTH_FRAMEBUFFER CLASS
THIS CLASS IS AN EXTENSION TO THE FRAMEBUFFER CLASS.
IT HOLDS ONLY THE DEPTH OF THE FINAL PIXELS
*******************************************************************************************************/

#pragma once
#include "Common.h"
#include "FrameBuffer.h"

class DepthFrameBuffer : public FrameBuffer{
public:
	DepthFrameBuffer(int _Width, int _Height, const char* fShader = "data/shader/screen.frag", const char* vShader = "data/shader/screen.vert");
protected:
	void InitializeFrameBuffer() override;

};