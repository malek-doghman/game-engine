#include "DirectionalLight.h"
#include "Camera.h"
#include "LightModel.h"

DirectionalLight::DirectionalLight() :Light(){
	lAttributes.type = DIRECTIONAL_LIGHT;
	lightModel = new LightModel("directionalLight.obj");
	ModelWorldMatrix = glm::rotate(ModelWorldMatrix, 120.0f, glm::vec3(0.0, 1.0, 0.0));
	ModelWorldMatrix = glm::rotate(ModelWorldMatrix, -40.0f, glm::vec3(1.0, 0.0, 0.0));
	vec4 tmp = ModelWorldMatrix * vec4(lDirection, 1.0);
	lDirection = glm::normalize(vec3(tmp / tmp.w));

	tmp = ModelWorldMatrix * vec4(lRight, 1.0);
	lRight = glm::normalize(vec3(tmp / tmp.w));

	lUp = glm::normalize(glm::cross(lRight, lUp));

	lightModel->InitializeDirection(ModelWorldMatrix);
	lightModel->makeTransformation(ModelWorldMatrix);

}


void DirectionalLight::Initialize(){

}

void DirectionalLight::Update(){
	Light::Update();
	UpdateOrthoMatrix();

}


/**********************************************************
UpdateOrthoMatrix():
Is responsible of constructing orthognal matrices for the
different slices of the cacaded shadow map
***********************************************************/
void DirectionalLight::UpdateOrthoMatrix(){
	oMatrices[0] = adjustPerspSubFrustum(0.1f, 200.0f, 0);
	oMatrices[1] = adjustPerspSubFrustum(200.0f, 600.0f, 1);
	oMatrices[2] = adjustPerspSubFrustum(600.0f, 1800.0f, 2);
	oMatrices[3] = adjustPerspSubFrustum(1800.0f, 4000.0f, 3);

}

/**********************************************************
adjustPerspSubFrustum():
Gets as an input the nearest and farthest distance from
camera's point of view and returns an orthogonal matrix
for a slice of the cascaded shadow map
***********************************************************/
mat4 DirectionalLight::adjustPerspSubFrustum(float vNearP, float vFarP, unsigned int i){
	vector<vec3> worldFrust = worldFrustum(vNearP, vFarP, i);
	mat4 OrthMatrix;
	if (worldFrust.size()>0){

		vec4 tmp = vMatrices[i] * vec4(worldFrust[0], 1.0);
		tmp = tmp / tmp.w;
		vec3 min = vec3(tmp);
		vec3 max = vec3(tmp);
		for (vec3 v : worldFrust){
			tmp = vMatrices[i] * vec4(v, 1.0);
			tmp = tmp / tmp.w;
			if (tmp.x < min.x){
				min.x = tmp.x;
			}
			if (tmp.y < min.y){
				min.y = tmp.y;
			}
			if (tmp.z < min.z){
				min.z = tmp.z;
			}

			if (tmp.x > max.x){
				max.x = tmp.x;
			}
			if (tmp.y > max.y){
				max.y = tmp.y;
			}
			if (tmp.z > max.z){
				max.z = tmp.z;
			}
		}

		float add = (250.0f*pow(3, i + 1));;
		OrthMatrix = glm::ortho(min.x, max.x, min.y, max.y, -max.z - add, -min.z);

	}
	return(OrthMatrix);
}

/**********************************************************
worldFrustum();
Gets as an input the nearest and farthest distance from
camera's point of view and returns the view frustum in
world space coordinates
***********************************************************/
vector<vec3> DirectionalLight::worldFrustum(float vNearP, float vFarP, unsigned int k){

	float cNearP = toClipDepth(vNearP);	//Convert depth from camera-viewSpace to camera-ClipSpace
	float cFarP = toClipDepth(vFarP);

	vector<vec3>viewFrustum;

	viewFrustum.push_back(vec3(-1.0, -1.0, cNearP));
	viewFrustum.push_back(vec3(1.0, -1.0, cNearP));
	viewFrustum.push_back(vec3(-1.0, 1.0, cNearP));
	viewFrustum.push_back(vec3(1.0, 1.0, cNearP));

	viewFrustum.push_back(vec3(-1.0, -1.0, cFarP));
	viewFrustum.push_back(vec3(1.0, -1.0, cFarP));
	viewFrustum.push_back(vec3(-1.0, 1.0, cFarP));
	viewFrustum.push_back(vec3(1.0, 1.0, cFarP));

	tmpPosition = -lDirection * 1000.0f;	//1000 is camera's far view distance, remember to fix !!!!
	centroid = vec3(0.0);

	for (unsigned int i = 0; i < viewFrustum.size(); i++){
		mat4 inv = Camera::getInvertedViewProjectionMatrix();
		vec4 out = inv*vec4(viewFrustum[i], 1.0);
		out = out / out.w;
		centroid += vec3(out);
		viewFrustum[i] = vec3(out.x, out.y, out.z);
	}
	centroid = centroid / 8.0f;

	tmpPosition += centroid;
	vMatrices[k] = glm::lookAt(tmpPosition, centroid, lUp);
	return(viewFrustum);
}


/**********************************************************
toClipDepth();
Convertes depth from view space to clip space
***********************************************************/
float DirectionalLight::toClipDepth(float viewDepth){
	vec4 tmp = vec4(Camera::getEyePos() + viewDepth * Camera::getDirection(), 1.0);
	vec4 v = Camera::getViewProjectionMatrix() * tmp;
	v.z = v.z / v.w;
	return(v.z);
}



/**********************************************************
getVPMatrix()
Returns the next orthogonal matrix for a different slice
***********************************************************/
mat4 DirectionalLight::getVPMatrix(){
	static int i = 0;
	i = i % 4;
	mat4 p = oMatrices[i];
	return(p * vMatrices[i++]);

}

/**********************************************************
getVPMatrix()
Returns the i ranked orthogonal matrix
***********************************************************/
mat4 DirectionalLight::getVPByIndex(unsigned int i){
	return(oMatrices[i] * vMatrices[i]);
}

void DirectionalLight::UpdateViewMatrix(){

}