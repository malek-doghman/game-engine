

/******************************************************************************************************
DIRECTIONAL_LIGHT CLASS
LIGHTING IS CALCULATED ONLY BASED ON LIGHT DIRECTION
DIRECTIONAL LIGHTS USES ORTHOGONAL PROJECTIONS FOR THEIR SHADOW MAP (SINCE THEY SIMULATE A SUNLIGHT)
SINCE CASCADED SHADOW MAPS ARE USED: EACH SLICE IS MAPPED TO ITS CORRESPONG ORTHOGONAL MATRIX
*******************************************************************************************************/


#pragma once
#include "Common.h"
#include "Light.h"

class DirectionalLight :public Light{
public:
	DirectionalLight();
	virtual void Initialize() override;
	virtual void Update() override;
	mat4 getVPMatrix() override;
	mat4 getVPByIndex(unsigned int i) override;
	void UpdateViewMatrix() override;
private:
	vector<vec3> worldFrustum(float vNearP, float vFarP, unsigned int i);
	float toClipDepth(float viewDepth);
	mat4 adjustPerspSubFrustum(float vNearP, float vFarP, unsigned int i);
	void UpdateOrthoMatrix();
	mat4 oMatrices[4];
	mat4 vMatrices[4];
	vec3 tmpPosition;
	vec3 centroid;
};