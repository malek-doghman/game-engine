#pragma once
#include "DynamicModel.h"
#include "Mesh.h"


DynamicModel::DynamicModel(string _fileName) :Model(_fileName)
{


}

void DynamicModel::Initialize(){
	for (Mesh* m : meshes){
		m->Initialize();
	}
}

void DynamicModel::Update(){
	for (Mesh* m : meshes){
		m->Update();
	}


}

void DynamicModel::Draw(ShaderProgram& shader, Light* l){
	for (Mesh* m : meshes){
		m->Draw(shader, l);
	}
}

void DynamicModel::DrawInstanced(GLsizei instanceCount, ShaderProgram& shader, Light* l){
	for (Mesh* m : meshes){
		m->DrawInstanced(instanceCount, shader, l);
	}
}

VIBuffer DynamicModel::getVertexIndexBuffer(){
	return(VIBuffer());
}
void DynamicModel::addVertexAttribute(GLuint index, GLint size, GLenum type, GLboolean normalized, GLsizei stride, const GLvoid *pointer, GLuint divisorCount){
	for (Mesh* m : meshes){
		m->addVertexAttribute(index, size, type, normalized, stride, pointer, divisorCount);
	}

}


DynamicModel::~DynamicModel(){

}