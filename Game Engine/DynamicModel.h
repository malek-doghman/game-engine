
/*******************************************************************************************************
DYNAMIC_MODEL CLASS
EACH MESH OF THIS OBJECT IS INDEPENDENT: IT HAS ITS OWN VIBUFFER,TEXTURES,...
THIS CLASS IS USED FOR DYNAMIC OBJECTS THAT WILL BE MOVING IN SPACE
*******************************************************************************************************/

#pragma once
#include "Model.h"

class DynamicModel :public Model{
public:
	DynamicModel(string _fileName);
	virtual void Initialize() override;
	virtual void Update() override;
	virtual void Draw(ShaderProgram& shader, Light* l = nullptr) override;
	void DrawInstanced(GLsizei instanceCount, ShaderProgram& shader, Light* l = nullptr);
	void addVertexAttribute(GLuint index, GLint size, GLenum type, GLboolean normalized, GLsizei stride, const GLvoid *pointer, GLuint divisorCount = 0);
	VIBuffer getVertexIndexBuffer() override;
	~DynamicModel();
private:

};