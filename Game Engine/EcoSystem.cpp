#include "EcoSystem.h"

#define max(a,b) a>b ? a : b;

#define RETRY_COUNT 2

vector<vec3>* EcoComponent::allPositions = nullptr;
vector<YawPitch>* EcoComponent::objAnglesR = nullptr;

void EcoComponent::Draw(ShaderProgram& shader, Light* l){
	model->DrawInstanced(count, shader, l);
}


void EcoComponent::InitializeStaticMembers(vector<vec3>* _Positions, vector<YawPitch>* _Angles){
	allPositions = _Positions;
	objAnglesR = _Angles;
}

void EcoComponent::Initialize(){
	this->model->Initialize();
	glGenBuffers(1, &this->objPositionsVB);
	glBindBuffer(GL_ARRAY_BUFFER, this->objPositionsVB);
	glBufferData(GL_ARRAY_BUFFER, this->count*sizeof(vec3), &allPositions->at(this->range.first), GL_STATIC_DRAW);
	this->model->addVertexAttribute(3, 3, GL_FLOAT, GL_FALSE, sizeof(vec3), 0, 1);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &this->objAngles);
	glBindBuffer(GL_ARRAY_BUFFER, this->objAngles);
	glBufferData(GL_ARRAY_BUFFER, this->count*sizeof(YawPitch), &this->objAnglesR->at(this->range.first), GL_STATIC_DRAW);
	this->model->addVertexAttribute(4, 2, GL_FLOAT, GL_FALSE, sizeof(YawPitch), 0, 1);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}

EcoSystem::EcoSystem(Terrain* _Terrain) : GameComponent(true, true), vTerrain(_Terrain)
{


}

YawPitch getAngles(const vec3& normal){
	vec3 tmp = glm::normalize(normal);
	float yawAngle = glm::acos(glm::dot(vec3(1.0, 0.0, 0.0), tmp));
	float pitchAngle = -glm::acos(glm::dot(vec3(0.0, 1.0, 0.0), tmp));
	return(YawPitch(yawAngle, pitchAngle));

}

void EcoSystem::populateEcoSystem(unsigned int objCount, float minRadius, ComponentType type){
	float offset = 10;
	if (type == ROCK){
		offset = 0;
	}
	float radius = max((vTerrain->getSurface() / objCount), minRadius);
	float terrainScale = vTerrain->getScale();
	for (unsigned int i = 0; i < objCount; i++){
		vec2 vPos = findValidPosition(terrainScale, radius, objPositions, type != ROCK ? RETRY_COUNT : 1);
		vec3 tmp = vec3(vPos.x, 0.0f, -vPos.y);
		vec3 position = vec3(vPos.x, vTerrain->getHeightAt(tmp) - offset, -vPos.y);
		YawPitch yawPitch;
		if (type == ROCK){
			vec3 norm = vTerrain->getNormalAt(tmp);
			//yawPitch = getAngles(norm);

			//position -= norm;
		}
		else{
			yawPitch.first = (static_cast<float>(rand()) / RAND_MAX) * 360 + vPos.x + vPos.y;
		}
		objAngles.push_back(yawPitch);
		objPositions.push_back(position);
	}
}



vec2 EcoSystem::findPosition(float _Scale){
	float x = (static_cast<float>(rand()) / RAND_MAX) * _Scale;
	float y = (static_cast<float>(rand()) / RAND_MAX) * _Scale;

	return(vec2(x, y));
}

bool EcoSystem::validPosition(const vec2& vPos, float minRadius, vector<vec3>& objPositions){
	for (const vec3& sPos : objPositions){
		if (glm::length(vec2(sPos) - vPos) < minRadius){
			return(false);
		}
	}
	return(true);
}

vec2 EcoSystem::findValidPosition(float _Scale, float _minRadius, vector<vec3>& objPositions, unsigned int retryCount){
	vec2 vPos;
	for (unsigned int i = 0; i < retryCount; i++){
		vPos = findPosition(_Scale);
		if (validPosition(vPos, _minRadius, objPositions)){
			return(vPos);
		}
	}
	return(vPos);
}


void EcoSystem::Initialize(){
	EcoComponent::InitializeStaticMembers(&objPositions, &objAngles);
	for (unsigned int i = 0; i < components.size(); i++){
		components[i]->Initialize();
	}

}

void EcoSystem::Update(){

}

void EcoSystem::Draw(ShaderProgram& shader, Light* light){
	for (EcoComponent* comp : components){
		comp->Draw(shader);
	}
}


void EcoSystem::addNewComponent(const char* modelName, unsigned int count, float minRadius, ComponentType type){
	EcoComponent* component = new EcoComponent();


	component->model = new DynamicModel(modelName);


	int last = objPositions.size();

	populateEcoSystem(count, minRadius, type);

	component->count = objPositions.size() - last;
	component->range = pair<unsigned int, unsigned int>(last, objPositions.size() - 1);

	component->type = type;

	components.push_back(component);
}