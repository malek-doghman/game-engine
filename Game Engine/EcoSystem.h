#pragma once
#include "Common.h"
#include "Terrain.h"
#include "StaticModel.h"
#include "DynamicModel.h"
#include "GameComponent.h"



enum ComponentType{
	PLANT,
	ROCK
};

typedef pair<float, float> YawPitch;

class EcoComponent{
public:

	DynamicModel* model;
	GLuint objPositionsVB, objAngles;
	ComponentType type;
	pair<unsigned int, unsigned int>range;
	unsigned int count;
	void Initialize();
	void Draw(ShaderProgram& shader, Light* l = nullptr);

	static void InitializeStaticMembers(vector<vec3>* _Position, vector<YawPitch>* _Angles);

private:
	static vector<vec3>* allPositions;
	static vector<YawPitch>* objAnglesR;
};

class EcoSystem : public GameComponent{
public:
	EcoSystem(Terrain* _Terrain);
	void Initialize() override;
	void Update() override;
	void Draw(ShaderProgram& shader, Light* l = nullptr) override;

	void addNewComponent(const char* modelName, unsigned int count, float minRadius, ComponentType type);

private:

	vector<EcoComponent*> components;
	Terrain* vTerrain;
	vector<vec3> objPositions;
	vector<YawPitch> objAngles;

	void populateEcoSystem(unsigned int objCount, float minRadius, ComponentType type);
	vec2 findPosition(float _Scale);
	bool validPosition(const vec2& vPos, float minRadius, vector<vec3>& objPositions);
	vec2 findValidPosition(float _Scale, float minRadius, vector<vec3>& objPositions, unsigned int retryCount);
};