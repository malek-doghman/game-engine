#include "FrameBuffer.h"
#include "WindowGL.h"
#include "ShaderProgram.h"
#include "Utility.h"
#include "glException.h"
#include "Camera.h"

VIBuffer FrameBuffer::rectangle;



FrameBuffer::FrameBuffer(int _Width, int _Height, const char* fShader, const char* vShader) :
width(_Width), height(_Height)
{
	rectangle = Utility::getRectangle();
	colorTexture = depthTexture = 0;
	//InitializeFrameBuffer();  //NOOB ALERT: you can't use pure virtual function in constructor and expect it to work correctly: this is because vtable are not initialized yet !
	setFilterShader(vShader, fShader);
	frameBufferBound = false;
}


void FrameBuffer::setFilterShader(const char* vShader, const char* fShader){

	shader.InsertShader(GL_VERTEX_SHADER, vShader);
	shader.InsertShader(GL_FRAGMENT_SHADER, fShader);
	shader.BuildProgram();

}

const ShaderProgram& FrameBuffer::getFilter(){
	if (shader.getProgram() == 0){
		throw(glException("FrameBuffer's shader not initialized !"));
	}
	return(shader);
}


/**********************************************************
UpdateFrameBuffer():
Update the whole framebuffer
***********************************************************/
void FrameBuffer::UpdateFrameBuffer(){

	bindFrameBuffer();
	glViewport(0, 0, width, height);

}

/**********************************************************
UpdateFrameBuffer():
Update a part of framebuffer based on startX,startY
***********************************************************/
void FrameBuffer::UpdateFrameBuffer(int startX, int startY, int xSize, int ySize){

	bindFrameBuffer();
	glViewport(startX, startY, xSize, ySize);

}


/**********************************************************
UpdateFrameBuffer():
Draws a filtered version of the current framebuffer
using the set shader.
Drawing occurs on the current active framebuffer
***********************************************************/
void FrameBuffer::DrawFrameBuffer(const ShaderProgram* externalFilter){
	glBindVertexArray(rectangle.VAO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, rectangle.indexBuffer);
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, colorTexture);

	if (externalFilter == nullptr){
		glUseProgram(shader.getProgram());
		shader.sendUniformData("oldWVP", Camera::getOldWVPMatrix());
		shader.sendUniformData("invertedWVP", Camera::getInvertedViewProjectionMatrix());
		shader.sendUniformData("enableMotionBlur", Camera::isRotated());
	}

	else{
		glUseProgram(externalFilter->getProgram());
	}

	//shader.sendUniformData("texelSize", glm::vec3((1.0f/1800),(1.0f/height),(1.0f/1800)));
	glDrawElements(GL_TRIANGLES, rectangle.indexCount, GL_UNSIGNED_INT, 0);
	glUseProgram(0);
	glBindVertexArray(0);
}


int FrameBuffer::checkFrameBufferStatus(){
	GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
	if (status != GL_FRAMEBUFFER_COMPLETE){
		return(FB_INCOMPLETE);
	}
	return(FB_COMPLETE);

}

void FrameBuffer::bindFrameBuffer(){
	if (!frameBufferBound){
		glBindFramebuffer(GL_FRAMEBUFFER, frameBuffer);
		frameBufferBound = true;
	}
}

void FrameBuffer::bindRead(){
	if (!frameBufferBound){
		glBindFramebuffer(GL_READ_FRAMEBUFFER, frameBuffer);
		frameBufferBound = true;
	}
}

void FrameBuffer::bindWrite(){
	if (!frameBufferBound){
		glBindFramebuffer(GL_DRAW_FRAMEBUFFER, frameBuffer);
		frameBufferBound = true;
	}
}

void FrameBuffer::unBindFrameBuffer(){
	if (frameBufferBound){
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
		glViewport(0, 0, WindowGL::GetWidth(), WindowGL::GetHeight());
		frameBufferBound = false;
	}
}

bool FrameBuffer::copyDepthFromFrameBuffer(FrameBuffer& srcFrameBuffer, GLint srcWidth, GLint srcHeight, GLint dstWidth, GLint dstHeight){
	this->bindWrite();
	srcFrameBuffer.bindRead();
	glEnable(GL_DEPTH_TEST);
	glDepthMask(GL_TRUE);
	glDepthFunc(GL_ALWAYS);
	glColorMask(GL_FALSE, GL_FALSE, GL_FALSE, GL_FALSE);
	glBlitFramebuffer(0, 0, srcWidth, srcHeight, 0, 0, dstWidth, dstHeight, GL_DEPTH_BUFFER_BIT, GL_NEAREST);	//This might not work for ATI cards
	GLenum status = glCheckFramebufferStatus(GL_DRAW_FRAMEBUFFER);
	glDisable(GL_DEPTH_TEST);
	glDepthMask(GL_FALSE);
	glDepthFunc(GL_LEQUAL);
	glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);
	this->unBindFrameBuffer();
	srcFrameBuffer.unBindFrameBuffer();
	if (status != GL_FRAMEBUFFER_COMPLETE){
		return(false);
	}
	return(true);
}


/**********************************************************
getColorTexture():
returns color texture id.
this method only works with CompleteFrameBuffer and
widht ColorFrameBuffer objects
An exception is thrown otherwise !
***********************************************************/
GLuint FrameBuffer::getColorTexture(){
	if (colorTexture == 0){
		throw(glException("Color texture uninitialized !"));
	}
	else{
		return(colorTexture);
	}
}