/*******************************************************************************************************
FRAMEBUFFER CLASS
A MOTHER CLASS
*******************************************************************************************************/

#pragma once

#include "Common.h"
#include "ShaderProgram.h"

#define FB_COMPLETE 1
#define FB_INCOMPLETE 0

class FrameBuffer{
public:
	FrameBuffer(int _Width, int _Height, const char* fShader = "data/shader/screen.frag", const char* vShader = "data/shader/screen.vert");
	void UpdateFrameBuffer();
	void UpdateFrameBuffer(int startX, int startY, int xSize, int ySize);
	void DrawFrameBuffer(const ShaderProgram* externalFilter = nullptr);
	void setFilterShader(const char* vShader, const char* fShader);
	const ShaderProgram& getFilter();
	bool copyDepthFromFrameBuffer(FrameBuffer& srcFrameBuffer, GLint srcWidth, GLint srcHeight, GLint dstWidth, GLint dstHeight);
	int checkFrameBufferStatus();
	void bindFrameBuffer();
	void bindRead();
	void bindWrite();
	void unBindFrameBuffer();
	GLuint getColorTexture();

protected:
	int width, height;
	GLuint frameBuffer;
	static VIBuffer rectangle;
	GLuint colorTexture;
	GLuint depthTexture;
	ShaderProgram shader;
	bool frameBufferBound;

	virtual void InitializeFrameBuffer() = 0;
};