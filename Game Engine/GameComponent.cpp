#include "GameComponent.h"



/**********************************************************
Active objects are updated
Active & Drawn objects are drawn
***********************************************************/

GameComponent::GameComponent(bool active_, bool drawable_) :active(active_), drawable(drawable_)
{

}


GameComponent::~GameComponent()
{

}


bool GameComponent::getDrawStatus(){
	return(drawable);
}



bool GameComponent::getActiveStatus(){
	return(active);
}


