

#pragma once
#include "Common.h"


class ShaderProgram;
class Light;

class GameComponent
{
public:
	GameComponent(bool active = false, bool drawable_ = false);
	virtual ~GameComponent();
	virtual void Initialize() = 0;
	virtual void Update() = 0;
	virtual void Draw(ShaderProgram& shader, Light* l = nullptr) = 0;	//Make shader Program constant for more security
	bool getActiveStatus();
	bool getDrawStatus();

protected:
	bool drawable;
	bool active;
};