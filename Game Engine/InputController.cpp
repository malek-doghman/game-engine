#include "InputController.h"
#include "WindowGL.h"
#include "Utility.h"


float InputController::elapsedTime = 1.0f;

Controls::Controls(){
	RightKey = Key::KEY_RIGHT;
	LeftKey = Key::KEY_LEFT;
	UpKey = Key::KEY_UP;
	DownKey = Key::KEY_DOWN;
	ForwardKey = Key::KEY_INSERT;
	BackwardKey = Key::KEY_DELETE;

	YawKey1 = Key::KEY_PAGE_UP;
	YawKey2 = Key::KEY_PAGE_DOWN;
	RollKey1 = Key::KEY_P;
	RollKey2 = Key::KEY_M;
	PitchKey1 = Key::KEY_HOME;
	PitchKey2 = Key::KEY_END;

}


InputController::InputController(bool _Rotate, bool _Translate, bool _MouseRotate, float _MovSpeed, float _RotSpeed, vec3 _fDirection, vec3 _UpDirection
	, vec3 _RightDirection) :
	rotate(_Rotate), translate(_Translate), mouseRot(_MouseRotate),
	forwardDirection(_fDirection), upDirection(_UpDirection), rightDirection(_RightDirection),
	movSpeed(_MovSpeed), rotSpeed(_RotSpeed)
{

}


bool InputController::getTransformation(mat4& _WorldMatrix)
{
	bool translation = false,
		rotation = false;
	mat4 translateMatrix;
	mat4 rotateMatrix;
	elapsedTime = WindowGL::getElapsedTime();
	float nMovSpeed = elapsedTime*movSpeed;
	float nRotSpeed = elapsedTime*rotSpeed;

	if (translate){
		translation = keyboardTranslate(nMovSpeed, translateMatrix);
	}

	if (rotate){
		if (mouseRot){
			mouseRotate(nRotSpeed, rotateMatrix);
		}
		else{
			rotation = keyboardRotate(nRotSpeed, rotateMatrix);
		}
	}

	if (rotation){
		vec4 tmp = rotateMatrix * vec4(rightDirection, 1.0);
		rightDirection = vec3(tmp / tmp.w);
		tmp = rotateMatrix *  vec4(upDirection, 1.0);
		upDirection = vec3(tmp / tmp.w);

		if (!Utility::isRightHanded(rightDirection, upDirection, forwardDirection)){
			forwardDirection = -glm::cross(rightDirection, upDirection);

		}
		else{
			forwardDirection = glm::cross(rightDirection, upDirection);
		}
		forwardDirection = glm::normalize(forwardDirection);
		upDirection = glm::normalize(upDirection);
		rightDirection = glm::normalize(rightDirection);
	}

	if (rotation || translation){
		if (rotation){
			mat3 r = mat3(vec3(_WorldMatrix[0]), vec3(_WorldMatrix[1]), vec3(_WorldMatrix[2]));
			mat3 rot = mat3(vec3(rotateMatrix[0]), vec3(rotateMatrix[1]), vec3(rotateMatrix[2]));
			mat3 result = rot * r;
			_WorldMatrix = mat4(vec4(result[0], 0.0), vec4(result[1], 0.0), vec4(result[2], 0.0), (_WorldMatrix[3]));
		}

		if (translation){
			_WorldMatrix = translateMatrix * _WorldMatrix;
		}
		return(true);
	}
	return(false);
}


void InputController::makeTransformation(const mat4& transform){
	vec4 tmp = transform * vec4(forwardDirection, 1.0);
	forwardDirection = glm::normalize(vec3(tmp / tmp.w));
	tmp = transform * vec4(rightDirection, 1.0);
	rightDirection = glm::normalize(vec3(tmp / tmp.w));
	upDirection = glm::normalize(glm::cross(rightDirection, forwardDirection));
}

bool InputController::keyboardTranslate(float nMovSpeed, mat4& translateMatrix){
	bool translation = false;

	if (glfwGetKey(WindowGL::GetWindowPointer(), keyControls.RightKey)){
		translateMatrix = glm::translate(translateMatrix, rightDirection*nMovSpeed);
		translation = true;
	}
	if (glfwGetKey(WindowGL::GetWindowPointer(), keyControls.LeftKey)){
		translateMatrix = glm::translate(translateMatrix, -rightDirection*nMovSpeed);
		translation = true;
	}

	if (glfwGetKey(WindowGL::GetWindowPointer(), keyControls.UpKey)){
		translateMatrix = glm::translate(translateMatrix, upDirection*nMovSpeed);
		translation = true;
	}

	if (glfwGetKey(WindowGL::GetWindowPointer(), keyControls.DownKey)){
		translateMatrix = glm::translate(translateMatrix, -upDirection*nMovSpeed);
		translation = true;
	}

	if (glfwGetKey(WindowGL::GetWindowPointer(), keyControls.ForwardKey)){
		translateMatrix = glm::translate(translateMatrix, forwardDirection*nMovSpeed);
		translation = true;
	}

	if (glfwGetKey(WindowGL::GetWindowPointer(), keyControls.BackwardKey)){
		translateMatrix = glm::translate(translateMatrix, -forwardDirection*nMovSpeed);
		translation = true;
	}
	return(translate);
}


bool InputController::keyboardRotate(float nRotSpeed, mat4& rotateMatrix){
	bool rotation = false;
	if (glfwGetKey(WindowGL::GetWindowPointer(), keyControls.YawKey1)){
		rotateMatrix = glm::rotate(rotateMatrix, nRotSpeed, vec3(0.0, 1.0, 0.0));
		rotation = true;
	}
	if (glfwGetKey(WindowGL::GetWindowPointer(), keyControls.YawKey2)){
		rotateMatrix = glm::rotate(rotateMatrix, -nRotSpeed, vec3(0.0, 1.0, 0.0));
		rotation = true;
	}

	if (glfwGetKey(WindowGL::GetWindowPointer(), keyControls.PitchKey1)){
		rotateMatrix = glm::rotate(rotateMatrix, nRotSpeed, rightDirection);
		rotation = true;
	}
	if (glfwGetKey(WindowGL::GetWindowPointer(), keyControls.PitchKey2)){
		rotateMatrix = glm::rotate(rotateMatrix, -nRotSpeed, rightDirection);
		rotation = true;
	}

	if (glfwGetKey(WindowGL::GetWindowPointer(), keyControls.RollKey1)){
		rotateMatrix = glm::rotate(rotateMatrix, nRotSpeed, forwardDirection);
		rotation = true;
	}
	if (glfwGetKey(WindowGL::GetWindowPointer(), keyControls.RollKey2)){
		rotateMatrix = glm::rotate(rotateMatrix, -nRotSpeed, forwardDirection);
		rotation = true;
	}
	return(rotation);
}


bool InputController::mouseRotate(float nRotSpeed, mat4& rotateMatrix){
	/*TODO*/
	return(false);
}

Controls& InputController::changeKey(){
	return(keyControls);
}



