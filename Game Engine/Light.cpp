#include "Light.h"
#include "WindowGL.h"
#include "Utility.h"
#include "Camera.h"

#define max(a,b) a<b ? b : a
#define min(a,b) a>b ? b : a

/******************************************************
Initializing Light Directions/Position
******************************************************/
Light::Light() : GameComponent(true, true){
	lRight = vec3(1.0, 0.0, 0.0);
	lUp = vec3(0.0, 1.0, 0.0);
	lDirection = vec3(0.0, 0.0, -1.0);
	lPosition = vec3(0.0, 0.0, 0.0);


	Initialize();
}

/**********************************************************
Initialize():
Depending on light's type(DirectionalLight/PointLight/
SpotLight) a projection matrix is initialized.
***********************************************************/
void Light::Initialize(){

}


/**********************************************************
Update():
Updating light's viewMatrix
A selected light is controlled according to controlLight()
method
***********************************************************/
void Light::Update(){
	UpdateViewMatrix();
	lightModel->Update();
	if (lightModel->isSelected()){
		ModelWorldMatrix = lightModel->getWorldMatrix();
		lDirection = glm::normalize(mat3(ModelWorldMatrix) * vec3(0.0, 0.0, -1.0));
		controlLight();
	}
}


/**********************************************************
Draw():
Draws the light model
***********************************************************/
void Light::Draw(ShaderProgram& shader, Light* l){
	lightModel->Draw();
}

void Light::UpdateViewMatrix(){
	vec3 position = vec3(ModelWorldMatrix * vec4(lPosition, 1.0));
	vec3 target = position + lDirection;
	viewMatrix = glm::lookAt(position, target, lUp);
}

void Light::setAttributes(vec3 position, float intensity, float radius, vec3 color){
	lAttributes.position = position;
	lAttributes.intensity = intensity;
	lAttributes.color = color;
}

void Light::setSelection(bool s){
	/*TODO*/
}

void Light::controlLight(){
	float elapsedTime = static_cast<float>(WindowGL::getElapsedTime());
	float movAmount = 99.0f * elapsedTime;
	float rotAmount = 19.0f * elapsedTime;
	float coeff = 1.01f*(elapsedTime + 1);

	if (glfwGetKey(WindowGL::GetWindowPointer(), GLFW_KEY_KP_ADD)){
		lAttributes.radius = lAttributes.radius *coeff;
	}
	if (glfwGetKey(WindowGL::GetWindowPointer(), GLFW_KEY_KP_SUBTRACT)){
		lAttributes.radius = lAttributes.radius / coeff;
	}

	if (glfwGetKey(WindowGL::GetWindowPointer(), GLFW_KEY_F1)){
		lAttributes.color.x = lAttributes.color.x*coeff;
	}

	if (glfwGetKey(WindowGL::GetWindowPointer(), GLFW_KEY_F2)){
		lAttributes.color.x = lAttributes.color.x / coeff;
	}

	if (glfwGetKey(WindowGL::GetWindowPointer(), GLFW_KEY_F3)){
		lAttributes.color.y = lAttributes.color.y*coeff;
	}

	if (glfwGetKey(WindowGL::GetWindowPointer(), GLFW_KEY_F4)){
		lAttributes.color.y = lAttributes.color.y / coeff;
	}

	if (glfwGetKey(WindowGL::GetWindowPointer(), GLFW_KEY_F5)){
		lAttributes.color.z = lAttributes.color.z*coeff;
	}

	if (glfwGetKey(WindowGL::GetWindowPointer(), GLFW_KEY_F6)){
		lAttributes.color.z = lAttributes.color.z / coeff;
	}

}

void Light::InitializeWorldMatrix_Translate(){
	ModelWorldMatrix = glm::translate(mat4(), lPosition);
}


LightAttribute Light::getAttributes(){
	lAttributes.position = vec3(ModelWorldMatrix * vec4(lPosition, 1.0));
	lAttributes.direction = lDirection;
	return(lAttributes);
}




mat4 Light::getViewMatrix(){
	return(viewMatrix);
}



mat4 Light::getVPMatrix(){
	return(mat4());
}

mat4 Light::getVPByIndex(unsigned int i){
	return(mat4());
}
