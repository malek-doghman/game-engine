
/*******************************************************************************************************
LIGHT CLASS
IS ACTUALLY AN INTERFACE(CANNOT BE INSTANTIATED) FOR ALL OTHER KIND OF LIGHT
EACH LIGHT HAS A DIRECTION AND A POSITION(EXCEPT FOR DIRECTIONAL LIGHT)
THERE ARE 3 TYPE OF LIGHT IN THIS CODE:
DIRECTIONAL LIGHT
POINT LIGHT
SPOT LIGHT
EACH TYPE OF LIGHT HAS ITS OWN CARATERSTICS REGARDING LIGHTING AND SHADOWS
DIRECTIONAL LIGHTS ARE CONFIGURED TO HAVE PRODUCE CASCADED SHADOW MAP
POINT/SPOT LIGHTS ARE CONFIGURED TO PRODUCE SINGLE PERSPECTIVE SHADOW MAPS
*******************************************************************************************************/

#pragma once
#include "GameComponent.h"
#include "Common.h"
#include "ShaderProgram.h"
#include "InputController.h"
#include "LightModel.h"


class Light : public GameComponent
{

public:
	Light();
	virtual void Initialize() = 0;
	virtual void Update() override;
	void Draw(ShaderProgram& shader, Light* l = nullptr) override;
	mat4 getViewMatrix();
	virtual mat4 getVPByIndex(unsigned int i);
	virtual mat4 getVPMatrix();

	void setAttributes(vec3 position, float intensity = 1.0f, float radius = 10.0f, vec3 color = vec3(1.0, 1.0, 1.0));
	LightAttribute getAttributes();

	void setSelection(bool s);


protected:
	vec3 lDirection;
	vec3 lPosition;
	vec3 lUp;
	vec3 lRight;

	LightModel* lightModel;

	mat4 viewMatrix;

	mat4 ModelWorldMatrix;
	mat4 rotateMatrix;

	LightAttribute lAttributes;

protected:
	virtual void UpdateViewMatrix();
	void controlLight();
	void InitializeWorldMatrix_Translate();
};