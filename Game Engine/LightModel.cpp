#include "LightModel.h"
#include "Camera.h"

ShaderProgram* LightModel::lightShader = nullptr;


LightModel::LightModel(string fileName) :StaticModel(fileName)
{
	intializeShader();
	Initialize();
	r = static_cast<float>(rand()) / RAND_MAX;
	g = static_cast<float>(rand()) / RAND_MAX;
	b = static_cast<float>(rand()) / RAND_MAX;
	modelColor = vec3(r, g, b);
}





void LightModel::Draw(){

	glBindVertexArray(bufferSpecs.VAO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, bufferSpecs.indexBuffer);


	glUseProgram(lightShader->getProgram());


	lightShader->sendUniformData("WVPMatrix", Camera::getViewProjectionMatrix() * worldMatrix);
	lightShader->sendUniformData("color", vec3(r, g, b));

	lightShader->sendUniformData("line", 0);


	glDrawElements(GL_TRIANGLES, bufferSpecs.indexCount, GL_UNSIGNED_INT, 0);

	if (selected){
		lightShader->sendUniformData("line", 1);
		glDrawElements(GL_LINE_LOOP, bufferSpecs.indexCount, GL_UNSIGNED_INT, 0);
	}


	glBindVertexArray(0);
	glUseProgram(0);

	if (selected){
		boundingBox.Draw();
	}
}

void LightModel::intializeShader(){
	if (lightShader == nullptr){
		lightShader = new ShaderProgram;
		lightShader->InsertShader(GL_VERTEX_SHADER, "data/shader/lightSprite.vert");
		lightShader->InsertShader(GL_FRAGMENT_SHADER, "data/shader/lightSprite.frag");
		lightShader->BuildProgram();
	}
}

const mat4& LightModel::getWorldMatrix(){
	return(worldMatrix);
}

bool LightModel::isSelected(){
	return(selected);
}