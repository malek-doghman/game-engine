#pragma once
#include "StaticModel.h"

class ShaderProgram;

class LightModel : public StaticModel{
public:
	LightModel(string fileName);
	void Draw();
	void intializeShader();
	const mat4& getWorldMatrix();
	bool isSelected();
private:
	float r, g, b;
	static ShaderProgram* lightShader;
	vec3 modelColor;
};