#include "Mesh.h"
#include "ShaderProgram.h"
#include "Camera.h"
#include "Textures.h"
#include "ShadowFrameBuffer.h"
#include "Light.h"
#include "Utility.h"
#include "Scene.h"


#define max(a,b) a<b ?b :a

/**********************************************************
Mesh():
Extracting Vertex/Index Data from file and store it
in Meshe's vectors(Vertices,Normals,Indices,...)
***********************************************************/
Mesh::Mesh(MeshData& meshData) : GameComponent(true, true), InputController(true, true){
	vector<glm::vec2>* textureCoords = new vector<glm::vec2>;

	for (unsigned int i = 0; i < meshData.faceData.pData.size(); i++){
		glm::vec3 vertexData = meshData.faceData.pData[i].v;
		Vertices.push_back(vertexData);

		//updating min/max vectors
		boundingBox.updateBounds(vertexData);

		Normals.push_back(meshData.faceData.pData[i].vn);
		textureCoords->push_back(meshData.faceData.pData[i].vt);
	}
	Indices = meshData.faceData.indices;
	uvChannels.push_back(textureCoords);
	mat = meshData.mat;
}


vector<glm::vec3>& Mesh::getVertices(){
	return(Vertices);
}

vector<unsigned int >& Mesh::getIndices(){
	return(Indices);
}
vector<glm::vec3>& Mesh::getNormals(){
	return(Normals);
}

vector<glm::vec3>& Mesh::getTangents(){
	return(Tangents);
}

vector<glm::vec3>& Mesh::getBiNormals(){
	return(biNormals);
}

vector<vector<glm::vec2>*>& Mesh::getUVChannels(){
	return(uvChannels);
}


/**********************************************************
Initialize():
Initialzing all GPU buffers and copy data from RAM to VRAM.
RAM Data is cleared as it will never be used again
by the client.
***********************************************************/
void Mesh::Initialize(){

	//Initialize Bounding Box
	boundingBox.Initialize();

	//updating boundries
	boundingBox.Update();

	//Computer a translation-matrix to origin 
	computeCenterMatrix(boundingBox.getBounds());

	glGenBuffers(1, &bufferSpecs.vertexBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, bufferSpecs.vertexBuffer);
	glBufferData(GL_ARRAY_BUFFER, Vertices.size() * sizeof(vec3), &Vertices[0], GL_STATIC_DRAW);

	glGenBuffers(1, &bufferSpecs.indexBuffer);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, bufferSpecs.indexBuffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(GLuint) * Indices.size(), &Indices[0], GL_STATIC_DRAW);

	bufferSpecs.indexCount = Indices.size();

	glGenVertexArrays(1, &bufferSpecs.VAO);
	glBindVertexArray(bufferSpecs.VAO);

	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(vec3), 0);

	if (Normals.size() > 0){
		glGenBuffers(1, &bufferSpecs.normalsBuffer);
		glBindBuffer(GL_ARRAY_BUFFER, bufferSpecs.normalsBuffer);
		glBufferData(GL_ARRAY_BUFFER, Normals.size()* sizeof(vec3), &Normals[0], GL_STATIC_DRAW);

		glEnableVertexAttribArray(2);
		glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, sizeof(vec3), 0);
	}

	glGenBuffers(1, &bufferSpecs.textureCoordsBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, bufferSpecs.textureCoordsBuffer);

	if (uvChannels[0]->size() > 0){
		vector<vec2>* v = uvChannels[0];
		glBufferData(GL_ARRAY_BUFFER, v->size() * sizeof(vec2), &v->at(0), GL_STATIC_DRAW);

		glEnableVertexAttribArray(1);
		glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, 0);


		glBindVertexArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	}

	ClearRAM();


}

void Mesh::Update(){
	boundingBox.isIntersected();
	selected = boundingBox.isSelected();
	if ((selected) && (getTransformation(transformationMatrix))){
		boundingBox.Update(transformationMatrix);
		updateWorldNormalMatrix(transformationMatrix);
	}
}


/**********************************************************
Draw():
Draws a single mesh
Depth texture is bound to to texture unit 1 !
***********************************************************/
void Mesh::Draw(ShaderProgram& shader, Light* l){
	glBindVertexArray(bufferSpecs.VAO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, bufferSpecs.indexBuffer);

	glUseProgram(shader.getProgram());
	shader.updateAllUniforms(mat);


	mat4 matrix = Camera::getViewProjectionMatrix() * worldMatrix;

	shader.sendUniformData("WVPMatrix", matrix);
	shader.sendUniformData("WorldMatrix", worldMatrix);
	shader.sendUniformData("NormalMatrix", normalMatrix);
	shader.sendUniformData("ViewMatrix", Camera::getViewMatrix());

	shader.sendUniformData("instanced", 0);

	if (l != nullptr){
		shader.sendUniformData("VPMatrixLight", l->getVPByIndex(0));
		shader.sendUniformData("VPL1", l->getVPByIndex(0));
		shader.sendUniformData("VPL2", l->getVPByIndex(1));
		shader.sendUniformData("VPL3", l->getVPByIndex(2));
		shader.sendUniformData("VPL4", l->getVPByIndex(3));
	}

	glDrawElements(GL_TRIANGLES, bufferSpecs.indexCount, GL_UNSIGNED_INT, 0);

	glBindVertexArray(0);
	glUseProgram(0);

	if (selected){
		boundingBox.Draw();
	}
}


void Mesh::InitializeDirection(const mat4& rotateMatrix){
	transformationMatrix = transformationMatrix * rotateMatrix;
	updateWorldNormalMatrix(transformationMatrix);
}


void Mesh::InitializePosition(const mat4& translateMatrix){
	transformationMatrix = translateMatrix * transformationMatrix;
	updateWorldNormalMatrix(transformationMatrix);
}

void Mesh::updateWorldNormalMatrix(const mat4& transform){
	worldMatrix = toPositionMatrix*transform * toCenterMatrix;
	normalMatrix = mat3(glm::transpose(glm::inverse(worldMatrix)));
}

void Mesh::DrawInstanced(GLsizei instanceCount, ShaderProgram& shader, Light* l){
	glDisable(GL_CULL_FACE);
	glBindVertexArray(bufferSpecs.VAO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, bufferSpecs.indexBuffer);


	glUseProgram(shader.getProgram());
	shader.updateAllUniforms(mat);


	mat4 matrix = Camera::getViewProjectionMatrix()  * worldMatrix;


	shader.sendUniformData("WVPMatrix", matrix);
	shader.sendUniformData("WorldMatrix", matrix);
	shader.sendUniformData("NormalMatrix", matrix);
	shader.sendUniformData("ViewMatrix", Camera::getViewMatrix());


	shader.sendUniformData("instanced", 1);

	if (l != nullptr){
		shader.sendUniformData("VPMatrixLight", l->getVPByIndex(0));
		shader.sendUniformData("VPL1", l->getVPByIndex(0));
		shader.sendUniformData("VPL2", l->getVPByIndex(1));
		shader.sendUniformData("VPL3", l->getVPByIndex(2));
		shader.sendUniformData("VPL4", l->getVPByIndex(3));
	}

	glDrawElementsInstanced(GL_TRIANGLES, bufferSpecs.indexCount, GL_UNSIGNED_INT, 0, instanceCount);

	glBindVertexArray(0);
	glUseProgram(0);

	if (selected){
		boundingBox.Draw();
	}
	glEnable(GL_CULL_FACE);
}


void Mesh::addVertexAttribute(GLuint index, GLint size, GLenum type, GLboolean normalized, GLsizei stride, const GLvoid *pointer, GLuint divisorCount){
	glBindVertexArray(bufferSpecs.VAO);
	glVertexAttribPointer(index, size, type, normalized, stride, pointer);
	glEnableVertexAttribArray(index);

	if (divisorCount > 0){
		glVertexAttribDivisor(index, divisorCount);
	}

	glBindVertexArray(0);
}



Material Mesh::getMaterial(){
	return(mat);
}



Mesh::~Mesh(){
	ClearVRAM();
}

void Mesh::ClearRAM(){
	Vertices.clear();
	Normals.clear();
	biNormals.clear();
	Tangents.clear();
	Indices.clear();
	for (vector<glm::vec2>* v : uvChannels){
		v->clear();
	}
	uvChannels.clear();
}


/**********************************************************
ClearVRAM():
Delete all gpu buffers and vao.
***********************************************************/
void Mesh::ClearVRAM(){
	glDeleteBuffers(1, &bufferSpecs.vertexBuffer);
	glDeleteBuffers(1, &bufferSpecs.indexBuffer);
	glDeleteBuffers(1, &bufferSpecs.normalsBuffer);
	glDeleteBuffers(1, &bufferSpecs.textureCoordsBuffer);

	glDeleteVertexArrays(1, &bufferSpecs.VAO);

}

/**********************************************************
ClearTextures():
Removes textures from the container.
Clears the texture set.
***********************************************************/
void Mesh::ClearTextures(){
	for (pair<GLuint, TextureType> t : textureFiles){
		glDeleteTextures(1, &t.first);
	}

	textureFiles.clear();
}





const BoundingBox& Mesh::getBoundingBox(){
	return(boundingBox);
}


void Mesh::computeCenterMatrix(const pair<vec3, vec3>&_minMax){
	vec3 centerPos = (_minMax.first + _minMax.second) / 2.0f;
	toCenterMatrix = glm::translate(mat4(), -centerPos);
	toPositionMatrix = glm::translate(mat4(), centerPos);
}