/*******************************************************************************************************
MESH CLASS
A MESH IS A PART OF MODEL
EACH MODEL CAN BE SUBDIVIDED INTO MANY MESHES
*******************************************************************************************************/


#pragma once
#include "Common.h"
#include "GameComponent.h"
#include "BoundingBox.h"
#include "InputController.h"
#include "MeshImporter.h"

class Camera;
class Light;

class Mesh : public GameComponent, public InputController{

public:
	Mesh(MeshData& meshData);
	vector<glm::vec3>& getVertices();
	vector<unsigned int >& getIndices();
	vector<glm::vec3>& getNormals();
	vector<glm::vec3>& getTangents();
	vector<glm::vec3>& getBiNormals();
	vector<vector<glm::vec2>*>& getUVChannels();
	virtual void Initialize() override;
	virtual void Update() override;
	virtual void Draw(ShaderProgram& shader, Light* l = nullptr) override;
	void DrawInstanced(GLsizei instanceCount, ShaderProgram& shader, Light* l = nullptr);
	void addVertexAttribute(GLuint index, GLint size, GLenum type, GLboolean normalized, GLsizei stride, const GLvoid *pointer, GLuint divisorCount = 0);
	Material getMaterial();
	const BoundingBox& getBoundingBox();
	void InitializeDirection(const mat4& rotateMatrix);
	void InitializePosition(const mat4& translateMatrix);
	void updateWorldNormalMatrix(const mat4& translateMatrix);
	~Mesh();

private:
	void ClearRAM();
	void ClearVRAM();
	void ClearTextures();
	void computeCenterMatrix(const pair<vec3, vec3>&_minMax);

	vector<glm::vec3>Vertices;
	vector<glm::vec3>Normals;
	vector<glm::vec3>biNormals;
	vector<glm::vec3>Tangents;
	vector<unsigned int>Indices;
	vector<vector<glm::vec2>*> uvChannels;

	VIBuffer bufferSpecs;

	std::set<pair<GLuint, TextureType>> textureFiles;

	BoundingBox boundingBox;

	mat4 worldMatrix,
		toCenterMatrix,
		toPositionMatrix,
		transformationMatrix;
	mat3 normalMatrix;
	Material mat;
	bool selected;

};