#include "MeshImporter.h"
#include "Textures.h"
#include "Utility.h"



MeshImporter::MeshImporter(string objName){
	loadVertexData(objName);
}

void MeshImporter::loadVertexData(string objName){
	fstream obj;
	obj.open(objName, ios::in);
	if (obj.is_open()){
		string tmp;
		bool materialsLoaded = false;
		while (obj >> tmp){
			processStream(tmp, obj);
		}
	}
	else{
		throw(glException("OBJ model not found !"));
	}
}

void MeshImporter::processStream(string& tmp, fstream& obj){
	static string lasMaterial;
	Utility::toLowerCase(tmp);
	//OBJ PARSING
	if (tmp.find("mtllib") != string::npos){
		string ttmp = getFullFileName(obj);
		loadAllMaterials(ttmp);
	}
	else if ((tmp == "vt")){
		vec2 vertex;
		obj >> vertex.x >> vertex.y;
		textureCoords.push_back(vertex);
	}
	else if ((tmp == "vn")){
		vec3 vertex;
		obj >> vertex.x >> vertex.y >> vertex.z;
		normals.push_back(vertex);

	}
	else if (tmp == "v"){
		vec3 vertex;
		obj >> vertex.x >> vertex.y >> vertex.z;
		vertices.push_back(vertex);

	}

	else if (tmp == "f"){
		for (int i = 0; i < 3; i++){
			string s;
			obj >> s;
			processFace(s);
		}
	}
	else if (tmp.find("usemtl") != string::npos){
		string ttmp;
		obj >> ttmp;
		MeshData p;
		p.mat = materials[ttmp];
		meshes.push_back(p);
	}



	//MTL PARSING
	else if (tmp == ("newmtl")){
		obj >> (lasMaterial);
		materials.insert(pair<string, Material>(lasMaterial, Material()));
	}
	else if (tmp == ("map_kd")){
		string textureName = getFullFileName(obj);
		const Texture* id = Textures::addTextureIntoRAM(textureName, DIFFUSE_TEXTURE);
		materials[lasMaterial].addTexture(id, DIFFUSE_TEXTURE);
	}
	else if (tmp == ("map_d")){
		string textureName = getFullFileName(obj);
		const Texture* id = Textures::addTextureIntoRAM(textureName, ALPHA_TEXTURE);
		materials[lasMaterial].addTexture(id, ALPHA_TEXTURE);
	}
	else if (tmp == ("ka")){
		vec3 color;
		obj >> color.r >> color.g >> color.b;
		materials[lasMaterial].setAmbientColor(color);
	}
	else if (tmp == ("kd")){
		vec3 color;
		obj >> color.r >> color.g >> color.b;
		materials[lasMaterial].setDiffuseColor(color);
	}
	else if (tmp == ("ks")){
		vec3 color;
		obj >> color.r >> color.g >> color.b;
		materials[lasMaterial].setSpecular(color.r);
	}
	obj.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
}

GLint MeshImporter::toValidInt(string& tmp){
	GLint attmp = atoi(tmp.c_str());
	if (attmp > 0){
		attmp--;
	}
	return(attmp);
}

void MeshImporter::processFace(string& s){
	bool vNorm = false, vTex = false;
	vector<GLint> res;
	string tmp;
	for (int i = 0; i < s.length(); i++){
		if (s[i] == '/'){
			res.push_back(toValidInt(tmp));
			tmp = "";
			vTex = true;
			if (s[i + 1] == '/'){
				i++;
				vTex = false;
				vNorm = true;
			}
			continue;
		}
		tmp += s[i];
	}

	res.push_back(toValidInt(tmp));
	if (res.size()>2)
		vTex = vNorm = true;



	FaceDataComponent fa;

	if (res[0] >= 0){
		res[0] = res[0] - vertices.size();
		if (vTex){
			res[1] = res[1] - textureCoords.size();
			if (vNorm){
				res[2] = res[2] - normals.size();
			}
		}
		else if (vNorm){
			res[1] = res[1] - normals.size();
		}
	}

	//NEED FIXING FOR GENERAL-PURPOSE USAGE
	GLint hash = 13 * 13 * 13 * (glm::abs(res[0]));
	if (vTex){
		hash += (glm::abs(res[1])) * 13 * 13;
		if (vNorm){
			hash += (glm::abs(res[2])) * 13;
			fa.setAttributes(vertices[vertices.size() + res[0]], textureCoords[textureCoords.size() + res[1]], normals[normals.size() + res[2]], hash, true, true);
		}
		else{
			fa.setAttributes(vertices[vertices.size() + res[0]], textureCoords[textureCoords.size() + res[1]], vec3(0.0), hash, true);
		}
	}
	else if (vNorm){
		hash += 13 * (11 + glm::abs(res[1]));
		fa.setAttributes(vertices[vertices.size() + res[0]], vec2(0.0), normals[normals.size() + res[1]], hash, false, true);
	}
	else{
		fa.setAttributes(vertices[vertices.size() + res[0]], vec2(0.0), vec3(0.0), hash);
	}

	meshes[meshes.size() - 1].faceData.insertComponent(fa);


}


void MeshImporter::loadAllMaterials(string fileName){
	Material m(fileName);
	string tmp;
	fstream f;
	f.open(fileName, std::ios::in);
	if (f.is_open()){
		while (f >> tmp){
			processStream(tmp, f);

		}
	}

}

vector<MeshData>& MeshImporter::getMeshes(){
	return(meshes);
}

FaceDataComponent::FaceDataComponent() :vTex(false), vNorm(false)
{

}

FaceDataComponent::FaceDataComponent(vec3 _v, vec2 _vt, vec3 _vn, GLint _hash) : v(_v), vn(_vn), vt(_vt), hashCode(_hash), vTex(true), vNorm(true)
{

}

FaceDataComponent::FaceDataComponent(vec3 _v, vec2 _vt, GLint _hash) : v(_v), vt(_vt), hashCode(_hash), vTex(true), vNorm(false){

}
FaceDataComponent::FaceDataComponent(vec3 _v, vec3 _vn, GLint _hash) : v(_v), vn(_vn), hashCode(_hash), vTex(false), vNorm(true){

}

void FaceDataComponent::setAttributes(vec3 _v, vec2 _vt, vec3 _vn, GLint _hash, bool _vTex, bool _vNor){
	v = _v;
	vTex = _vTex; vNorm = _vNor;
	hashCode = _hash;
	if (vTex)
		vt = _vt;
	if (vNorm)
		vn = _vn;
}


bool FaceDataComponent::operator == (const FaceDataComponent& t)const{
	return(hashCode == t.hashCode);
}


void FaceData::insertComponent(const FaceDataComponent& c){
	int old = tData.size();
	typedef unordered_map<FaceDataComponent, GLuint>::iterator mIt;
	mIt it = tData.find(c);
	if (it == tData.end()){
		indices.push_back(pData.size());
		tData.insert(pair<FaceDataComponent, GLuint>(c, pData.size()));
		pData.push_back(c);
	}
	else{
		indices.push_back(it->second);
	}

}

string MeshImporter::getFullFileName(fstream& f){
	bool b = false;
	string name;
	string tmp;
	while (!extensionExist(name)){
		if (b)
			name += " ";
		else
			b = true;

		f >> tmp;
		name += tmp;

	}
	return(name);
}


bool MeshImporter::extensionExist(string& tmp){
	vector<string> extensions = { ".png", ".tga", ".jpeg", ".jpg", ".bmp", ".tiff", ".mtl" };
	for (unsigned int i = 0; i < extensions.size(); i++){
		if (tmp.find(extensions[i]) != string::npos){
			return(true);
		}
	}
	return(false);
}