#pragma once
#include "Common.h"
#include "Model.h"

struct FaceDataComponent{
	FaceDataComponent();
	FaceDataComponent(vec3 _v, vec2 _vt, vec3 _vn, GLint _hash);
	FaceDataComponent(vec3 _v, vec2 _vt, GLint _hash);
	FaceDataComponent(vec3 _v, vec3 _vn, GLint _hash);
	void setAttributes(vec3 _v, vec2 _vt, vec3 _vn, GLint _hash, bool _vTex = false, bool _vNor = false);
	bool operator==(const FaceDataComponent& t)const;

	bool vTex, vNorm;
	vec3 v, vn;
	vec2 vt;
	GLint hashCode;
};

template <>
struct hash<FaceDataComponent>
{
	size_t operator()(FaceDataComponent const & x) const
	{
		return ((std::hash<int>()(x.hashCode)) * 13);
	}
};

class FaceData{
public:
	void insertComponent(const FaceDataComponent& c);
	unordered_map<FaceDataComponent, GLuint> tData;
	vector<FaceDataComponent> pData;
	vector<GLuint>indices;
};

struct MeshData{
	FaceData faceData;
	Material mat;
	map<string, GLchar*> textureData;
};


class MeshImporter{
public:
	MeshImporter(string fileName);
	vector<MeshData>& getMeshes();

private:
	void loadVertexData(string fileName);
	void loadAllMaterials(string fileName);
	void processStream(string& tmp, fstream& obj);
	void processFace(string& s);
	GLint toValidInt(string& s);
	string getFullFileName(fstream& f);
	bool extensionExist(string& tmp);

	vector<MeshData> meshes;
	vector<vec3>vertices;
	vector<vec2>textureCoords;
	vector<vec3>normals;

	map<string, Material>materials;

};