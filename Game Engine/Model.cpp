#include "Model.h"
#include "Mesh.h"
#include "glException.h"
#include "Camera.h"
#include "Textures.h"
#include "ShaderProgram.h"
#include"LightShadow.h"
#include "MeshImporter.h"


/**********************************************************
Model():
Extract all models meshes
Imports all mesh's data into RAM
***********************************************************/
Model::Model(string fileName_) :GameComponent(true, true){
	MeshImporter importer(fileName_);
	vector<MeshData> _meshes = importer.getMeshes();
	for (unsigned int i = 0; i < _meshes.size(); i++){
		Mesh* mesh = new Mesh(_meshes[i]);
		this->meshes.push_back(mesh);
	}

}


void Model::clearMeshes(){
	for (unsigned int i = 0; i < meshes.size(); i++){
		delete(meshes[i]);
	}
	meshes.clear();
}

Model::~Model(){
	clearMeshes();
}