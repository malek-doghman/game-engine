/*******************************************************************************************************
MODEL CLASS:
A MODEL IS A GROUP OF MESHES:
THERE ARE TWO TYPES OF MODELS IN THIS CODE:	STATIC MODELS & DYNAMIC MODELS
THE INTENTION BEHIND MAKING STATIC MODELS IS TO GROUP AS MANY MODELS AS POSSIBLE IN A SINGLE VBO
THIS WAY WE REDUCE THE NUMBER OF DRAW CALLS AND THUS IMPROVING THE FRAMERATE
THE PROBLEM WITH STATIC MODELS THAT WE CAN'T EASILY MOVE THEM SINCE THEY ARE ALL GROUPED IN A SINGLE
VBO(THERE COULD BE A WAY TO DO IT THOUGH)
THUS THERE IS THE NEED OF USING DYNAMIC MODELS WICH CAN BE EASILY MANIPULATED
*******************************************************************************************************/

#pragma once
#include "Common.h"
#include "glException.h"
#include "GameComponent.h"
#include "BoundingBox.h"


class Mesh;
class Camera;
class Light;



class Model : public GameComponent{
public:
	Model(string fileName_);
	virtual void Initialize() = 0;
	virtual void Update() = 0;
	virtual void Draw(ShaderProgram& shader, Light* l = nullptr) = 0;
	virtual VIBuffer getVertexIndexBuffer() = 0;
	~Model();

protected:


	void clearMeshes();

	vector<Mesh* >meshes;
	vector<unsigned int>meshIndices;

};