#include "Plane.h"


Plane::Plane(vec3 _p1, vec3 _p2, vec3 _p3){
	pts.push_back(_p1);
	pts.push_back(_p2);
	pts.push_back(_p3);
	findMinMax();
}

bool Plane::isRayIntersected(vec3& intersectionWithRay, vec3 Rd, vec3 R0) const{
	vec3 normal = glm::cross((pts[1] - pts[0]), (pts[2] - pts[1]));
	float d = -(glm::dot(normal, pts[0]));
	float Vd = glm::dot(normal, Rd);
	if (Vd == 0){
		return(false);
	}
	else{

		float V0 = -(glm::dot(normal, R0) + d);
		float t = V0 / Vd;

		intersectionWithRay = R0 + vec3(Rd.x*t, Rd.y*t, Rd.z*t);
		return(true);
		
	}
}

void Plane::transformMat4x4(const glm::mat4& transformMat){
	vec4 t = transformMat * vec4(pts[0], 1.0);
	pts[0] = vec3(t / t.w);

	t = transformMat * vec4(pts[1], 1.0);
	pts[1] = vec3(t / t.w);

	t = transformMat * vec4(pts[2], 1.0);
	pts[2] = vec3(t / t.w);

	findMinMax();
}


void Plane::findMinMax(){
	min = max = pts[0];


	for (vec3 p : pts){
		if (p.x < min.x){
			min.x = p.x;
		}
		if (p.y < min.y){
			min.y = p.y;
		}
		if (p.z < min.z){
			min.z = p.z;
		}

		if (p.x > max.x){
			max.x = p.x;
		}
		if (p.y > max.y){
			max.y = p.y;
		}
		if (p.z > min.z){
			max.z = p.z;
		}

	}

}


bool Plane::inRectangle(const vec3& v)const{
	return((v.x >= min.x - 0.01f) && (v.y >= min.y - 0.01f) && (v.z >= min.z - 0.01f) && (v.x <= max.x + 0.01f) && (v.y <= max.y + 0.01f) && (v.z <= max.z + 0.01f));
}