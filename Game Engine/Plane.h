#pragma once
#include "Common.h"


class Plane{
public:
	Plane(vec3 _p1, vec3 _p2, vec3 _p3);
	bool isRayIntersected(vec3& intersectionRay, vec3 Rd, vec3 R0) const;
	void transformMat4x4(const glm::mat4&);
	vec3 returnIntersection();
	void findMinMax();
	bool inRectangle(const vec3& v)const;
private:
	vector<vec3> pts;
	vec3 min, max;
};