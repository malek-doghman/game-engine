#include "PointLight.h"
#include "WindowGL.h"
#include "LightModel.h"

/*
THIS CLASS IS NOT READY YET:  IT'S FULL OF CRAP !!!!
*/

PointLight::PointLight() :Light(){
	lAttributes.type = POINT_LIGHT;
	Initialize();
	lightModel = new LightModel("pointLight.obj");
}

void PointLight::Initialize(){
	InitializePerspectiveMatrix();
}

void PointLight::Update(){
	Light::Update();
	UpdateVPMatrix();
}

/**********************************************************
InitializePerspectiveMatrix():
This matrix isn't correct because objects behind
the point light will not be seen, as a result shadows
behind the light will be incorrect.
***********************************************************/
void PointLight::InitializePerspectiveMatrix(){
	perspectiveMatrix = glm::perspective(180.0f / WindowGL::GetAspectRatio(), WindowGL::GetAspectRatio(), 50.1f, 1000.0f);

}


void PointLight::UpdateVPMatrix(){
	VPMatrix = perspectiveMatrix * viewMatrix;
}

mat4 PointLight::getVPMatrix(){
	return(VPMatrix);
}

mat4 PointLight::getVPByIndex(unsigned int i){
	return(getVPMatrix());
}
