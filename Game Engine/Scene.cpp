#include "Scene.h"
#include "Model.h"
#include "StaticModel.h"
#include "DynamicModel.h"
#include "Camera.h"
#include "BoundingBox.h"


Scene::Scene(){

}

Scene::Scene(vector<string> staticModels, vector<string> dynamicModels) :GameComponent(true, true){
	for (string s : staticModels){
		insertStaticModel(s);
	}

	for (string s : dynamicModels){
		insertDynamicModel(s);
	}
}


void Scene::Initialize(){
	for (pair<string, Model*> p : models){
		p.second->Initialize();
	}
}
void Scene::Update(){
	BoundingBox::InitilaizeClosest();
	for (pair<string, Model*> p : models){
		p.second->Update();
	}
}
void Scene::Draw(ShaderProgram& shader, Light* l){
	for (pair<string, Model*> p : models){
		p.second->Draw(shader, l);
	}
}

void Scene::insertStaticModel(string fileName){
	Model* model = nullptr;
	model = new StaticModel(fileName);
	if (model != nullptr){
		models.insert(pair<string, Model*>(fileName, model));
	}
	else{
		throw(glException("Model not loaded !"));
	}
}

void Scene::insertDynamicModel(string fileName){
	Model* model = nullptr;
	model = new DynamicModel(fileName);
	if (model != nullptr){
		models.insert(pair<string, Model*>(fileName, model));
	}
	else{
		throw(glException("Model not loaded !"));
	}
}

void Scene::removeModel(string fileName){
	map<string, Model*>::iterator iterator = models.find(fileName);
	if (iterator != models.end()){
		delete(iterator->second);
		models.erase(iterator);
	}
	else{
		throw(glException("Model is not loaded in scene"));
	}
}

