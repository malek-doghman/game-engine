/*******************************************************************************************************
SCENE CLASS:
A SCENE HOLDS A GROUP OF STATIC MODEL AND DYNAMIC MODELS WITHIN A VECTOR
EACH MODEL IS THEN UPDATED AND DRAWN
*******************************************************************************************************/


#pragma once
#include "Common.h"
#include "GameComponent.h"
#include "BoundingBox.h"

class Model;
class Scene :public GameComponent{
public:
	Scene();
	Scene(vector<string> staticModels, vector<string> dynamicModels = vector<string>());
	void Initialize() override;
	void Update() override;
	void Draw(ShaderProgram& shader, Light* l = nullptr) override;
	void insertDynamicModel(string fileName);
	void insertStaticModel(string fileName);
	void removeModel(string fileName);
private:
	map<string, Model*> models;
};


