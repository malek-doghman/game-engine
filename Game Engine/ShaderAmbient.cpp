#include "ShaderAmbient.h"

ShaderAmbient::ShaderAmbient(float intensity_) :ShaderProgram(), intensity(intensity_){
	InsertShader(GL_VERTEX_SHADER, "data/shader/scene.vert");
	InsertShader(GL_FRAGMENT_SHADER, "data/shader/forwardAmbient.frag");
	BuildProgram();
}

void ShaderAmbient::updateAllUniforms(const Material& m){
	sendUniformData("ambientColor", m.getAmbientColor());
	sendUniformData("intensity", intensity);
	m.bindTextures(this);
}

void ShaderAmbient::setIntensity(float f){
	intensity = f;
}