/*******************************************************************************************************
SHADER_AMBIENT CLASS:
IS A SUB-CLASS OF SHADER_PROGRAM: SINCE AMBIENT SHADER HAS ITS OWN SET OF UNIFORM VARIABLES
THAT HAS TO	BE UPADATED EACH DRAW CALL A SPECIAL CLASS HAS BEEN DECLARED FOR THEM :)
*******************************************************************************************************/


#pragma once
#include "ShaderProgram.h"


class ShaderAmbient : public ShaderProgram{
public:
	ShaderAmbient(float intensity_);
	void updateAllUniforms(const Material& m) override;
	void setIntensity(float f);

private:
	float intensity;
};