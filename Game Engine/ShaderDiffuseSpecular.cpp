#include "ShaderDiffuseSpecular.h"
#include "Camera.h"

ShaderDiffuseSpecular::ShaderDiffuseSpecular() :ShaderProgram(){
	InsertShader(GL_VERTEX_SHADER, "data/shader/scene.vert");
	InsertShader(GL_FRAGMENT_SHADER, "data/shader/forwardDiffuseSpecular.frag");
	BuildProgram();
}

void ShaderDiffuseSpecular::updateAttributes(const LightAttribute& l){
	attributes = l;
	attributes.eyePos = Camera::getEyePos();

}

void ShaderDiffuseSpecular::updateAllUniforms(const Material& m){
	sendUniformData("position", attributes.position);
	sendUniformData("color", attributes.color);
	sendUniformData("direction", attributes.direction);

	sendUniformData("eyePos", attributes.eyePos);
	sendUniformData("diffuseColor", m.getDiffuseColor());
	sendUniformData("intensity", attributes.intensity);
	sendUniformData("radius", attributes.radius);
	sendUniformData("lightType", attributes.type);

	sendUniformData("shininess", m.getSpecular());

	m.bindTextures(this);
}