/*******************************************************************************************************
SHADER_DIFFUSE_SPECULAR CLASS:
IS A SUB-CLASS OF SHADER_PROGRAM: SINCE DIFFUSE SHADER HAS ITS OWN SET OF UNIFORM VARIABLES
THAT HAS TO	BE UPADATED EACH DRAW CALL A SPECIAL CLASS HAS BEEN DECLARED FOR THEM :)
*******************************************************************************************************/

#pragma once
#include "ShaderProgram.h"

class ShaderDiffuseSpecular : public ShaderProgram{
public:
	ShaderDiffuseSpecular();
	void updateAllUniforms(const Material& m) override;
	virtual void updateAttributes(const LightAttribute& l) override;

private:
	LightAttribute attributes;
};