#include "ShaderProgram.h"
#include "ShadowFrameBuffer.h"
#include "glException.h"
#include "WindowGL.h"

map<pair<string, string>, GLint> ShaderProgram::allShaders;


ShaderProgram::ShaderProgram() :builtProgram(0)
{
}

ShaderProgram::~ShaderProgram(){
	typedef map<pair<string, string>, GLint>::iterator Iterator;
	glDeleteProgram(this->builtProgram);
	if (allShaders.size()>0)
		for (Iterator it = allShaders.begin(); it != allShaders.end(); it++){
			if (it->second == this->builtProgram){
				allShaders.erase(it);
				break;
			}
		}
}


string ShaderProgram::readFile(string fileName){
	fstream file;
	string shaderTxt = "";
	file.open(&fileName[0], ios::in);

	if (file.is_open()){
		string tmp;
		while (std::getline(file, tmp)){
			shaderTxt = shaderTxt + tmp + "\n";
		}
		file.close();
		return(shaderTxt);
	}
	else{
		throw(glException("Unable to locate shader file !"));
	}

}

GLuint ShaderProgram::getProgram() const{
	return(builtProgram);
}

/*********************************************************************************************
Inserting a shader inside shader list shaderList vector
*********************************************************************************************/
void ShaderProgram::InsertShader(GLenum shaderType, string shaderName){
	shader_t s;
	s.fileName = shaderName;
	s.shaderType = shaderType;
	shaderList.push_back(s);
}

/*********************************************************************************************
This method compiles all added shaders that resides inside shaderList vector
Compile errors are thrown !
*********************************************************************************************/

void ShaderProgram::CompileAllShaders(){
	for (shader_t shader : shaderList){
		GLuint wShader = glCreateShader(shader.shaderType);
		string src = readFile(shader.fileName);
		const GLchar* cSrc = &src[0];
		GLint size = src.size();
		glShaderSource(wShader, 1, &cSrc, &size);
		glCompileShader(wShader);
		GLint status;
		glGetShaderiv(wShader, GL_COMPILE_STATUS, &status);
		if (status == GL_FALSE){
			glGetShaderiv(wShader, GL_INFO_LOG_LENGTH, &size);
			string error;
			error.resize(size);
			glGetShaderInfoLog(wShader, size, &size, &error[0]);
			switch (shader.shaderType){
			case(GL_VERTEX_SHADER) :
				error = "Vertex Shader :\n" + error;
				break;
			case(GL_FRAGMENT_SHADER) :
				error = "Fragment Shader :\n" + error;
				break;
			default:
				break;
			}
			std::cout << shader.fileName + "\n" + error;
			throw(glException(error));
		}
		else{
			compiledShaders.push_back(wShader);
		}
	}


}

/*********************************************************************************************
BuildProgram() method is responsible for:
1 - Compiling Shaders
2 - Attaching Shaders
3 - Linking and building a program
If the same shader has been created before it will replace the program ID variable with
the previously generated shader program
Linking errors will be thrown when building the shader !
*********************************************************************************************/

void ShaderProgram::BuildProgram(){
	GLint shader = findShader();
	if (shader < 0){
		CompileAllShaders();
		builtProgram = glCreateProgram();
		for (GLuint shader : compiledShaders){
			glAttachShader(builtProgram, shader);
			glDeleteShader(shader);
		}
		glLinkProgram(builtProgram);
		GLint status;
		glGetProgramiv(builtProgram, GL_LINK_STATUS, &status);
		if (status == GL_FALSE){
			GLint size;
			glGetProgramiv(builtProgram, GL_INFO_LOG_LENGTH, &size);
			string msg;
			msg.resize(size);
			glGetProgramInfoLog(builtProgram, size, &size, &msg[0]);
			std::cout << msg;
			throw(glException(msg));
		}

		pair<string, string> p(shaderList[0].fileName, shaderList[1].fileName);
		if (shaderList[0].shaderType != GL_VERTEX_SHADER){
			p.first = shaderList[1].fileName;
			p.second = shaderList[0].fileName;
		}

		allShaders.insert(pair<pair<string, string>, GLint>(p, static_cast<GLint>(builtProgram)));
		shaderList.clear();
	}
	else{
		builtProgram = shader;
	}
}

/*********************************************************************************************
This method returns uniform location inside the shader
Unknown location will be stored in a map
*********************************************************************************************/

GLint ShaderProgram::findLocation(string uniformName){
	GLint uLocation;
	std::map<string, GLint>::iterator Iterator = uniformLocations.begin();
	Iterator = uniformLocations.find(uniformName);
	if (Iterator == uniformLocations.end()){
		string s = uniformName;
		uLocation = glGetUniformLocation(builtProgram, s.c_str());

		if (uLocation == -1){
			//throw(glException("Unable to locate uniform data or the uniform variable is not being used"));
		}
		pair<string, GLint> p = pair<string, GLint>(s, uLocation);
		uniformLocations.insert(p);
	}
	else{
		uLocation = Iterator->second;
	}
	return(uLocation);
}


/*********************************************************************************************
These methods allows to send uniform data to shaders:
unknown uniform locations will be stored in a map to prevent from lookind for their locations
each time we need them
*********************************************************************************************/

void ShaderProgram::sendUniformData(string uniformName, int value){
	GLint uLocation = findLocation(uniformName);
	glUniform1i(uLocation, value);
}

void ShaderProgram::sendUniformData(string uniformName, GLuint value){
	GLint uLocation = findLocation(uniformName);
	glUniform1i(uLocation, value);
}

void ShaderProgram::sendUniformData(string uniformName, GLfloat value){
	GLint uLocation = findLocation(uniformName);
	glUniform1f(uLocation, value);
}

void ShaderProgram::sendUniformData(string uniformName, GLdouble value){
	GLint uLocation = findLocation(uniformName);
	glUniform1f(uLocation, static_cast<float>(value));
}

void ShaderProgram::sendUniformData(string uniformName, vec2& v){
	GLint uLocation = findLocation(uniformName);
	glUniform2fv(uLocation, 1, &v[0]);
}

void ShaderProgram::sendUniformData(string uniformName, vec3& v){
	GLint uLocation = findLocation(uniformName);
	glUniform3fv(uLocation, 1, &v[0]);
}

void ShaderProgram::sendUniformData(string uniformName, vec4& v){
	GLint uLocation = findLocation(uniformName);
	glUniform4fv(uLocation, 1, &v[0]);
}

void ShaderProgram::sendUniformData(string uniformName, mat3& m){
	GLint uLocation = findLocation(uniformName);
	glUniformMatrix3fv(uLocation, 1, GL_FALSE, &m[0][0]);
}

void ShaderProgram::sendUniformData(string uniformName, mat4& m){
	GLint uLocation = findLocation(uniformName);
	glUniformMatrix4fv(uLocation, 1, GL_FALSE, &m[0][0]);
}

void ShaderProgram::updateAllUniforms(const Material& m){

}

void ShaderProgram::updateAttributes(const LightAttribute& l){

}


/*********************************************************************************************
This method will return the built shader program if it has been generated before by the client
to prevent from building the same shader program many times.
*********************************************************************************************/

GLint ShaderProgram::findShader(){
	GLint val = -1;
	map<pair<string, string>, GLint >::iterator it = allShaders.begin();
	pair<string, string>p;

	for (unsigned int i = 0; i < 2; i++){
		if (shaderList[i].shaderType == GL_VERTEX_SHADER){
			p.first = shaderList[i].fileName;
		}
		else{
			p.second = shaderList[i].fileName;
		}
	}


	it = allShaders.find(p);
	if (it != allShaders.end()){
		return(it->second);
	}
	else{
		return(-1);
	}
}

LightAttribute::LightAttribute(){
	direction = vec3(0, 0, -1);
	position = vec3(0);
	color = vec3(1.940, 0.864, 0.270);
	eyePos = vec3(0.0);
	intensity = 1.0f;
	radius = 500.0;
	type = SPOT_LIGHT;
}

LightAttribute& LightAttribute::operator=(const LightAttribute& l){
	direction = l.direction;
	position = l.position;
	color = l.color;
	intensity = l.intensity;
	radius = l.radius;
	type = l.type;
	return(*this);
}



Material::Material(){
	specular = 0.0f;
	ambientColor = diffuseColor = vec3(1.0);
}

Material::Material(const Material& m){
	specular = m.getSpecular();
	ambientColor = m.getAmbientColor();
	diffuseColor = m.getDiffuseColor();
	materialName = m.getMaterialName();
	textureFiles = m.getTextures();
}

Material::Material(string name) : materialName(name){
	Material();
}

GLfloat Material::getSpecular() const{
	return(specular);
}

void Material::setSpecular(GLfloat _specular){
	specular = _specular;
}

vec3 Material::getAmbientColor() const{
	return(ambientColor);
}

void Material::setAmbientColor(vec3 _amb){
	ambientColor = _amb;
}

vec3 Material::getDiffuseColor() const{
	return(diffuseColor);
}

void Material::setDiffuseColor(vec3 _amb){
	diffuseColor = _amb;
}

void Material::addTexture(const Texture* texture, TextureType ty){
	textureFiles.insert(pair<const Texture*, TextureType>(texture, ty));
}

string Material::getMaterialName() const{
	return(materialName);
}
std::set<pair<const Texture*, TextureType>> Material::getTextures()const {
	return(textureFiles);
}

void Material::bindTextures(ShaderProgram* shader)const{
	int uniformTextureType = 0x0;
	if (textureFiles.size() > 0){
		for (pair<const Texture*, TextureType>p : textureFiles){
			GLuint tIndex = (p.first->id);
			if (tIndex < 1){
				throw (glException("Texture is not loaded into VRAM or not found !"));
			}
			switch (p.second){
			case DIFFUSE_TEXTURE:
				glActiveTexture(GL_TEXTURE0);
				uniformTextureType = uniformTextureType | 0x1;
				break;
			case SPECULAR_TEXTURE:
				glActiveTexture(GL_TEXTURE2);
				uniformTextureType = uniformTextureType | 0x2;
				break;
			case ALPHA_TEXTURE:
				glActiveTexture(GL_TEXTURE3);
				uniformTextureType = uniformTextureType | 0x4;
				break;
			case NORMALS_TEXTURE:
				glActiveTexture(GL_TEXTURE4);
				uniformTextureType = uniformTextureType | 0x8;
				break;
			default:
				break;
			}
			glBindTexture(GL_TEXTURE_2D, tIndex);
		}
		shader->sendUniformData("textureType", uniformTextureType);

		//Binding Shadow Texture
		ShadowFrameBuffer::bindReadFB(1);
	}
}


void VIBuffer::DrawElements(GLFWwindow* window, ShaderProgram& shader, GLenum mode, GLvoid* indices, void(*loadAdditionalData)()){

	glBindVertexArray(VAO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBuffer);
	glUseProgram(shader.getProgram());

	if ((loadAdditionalData != nullptr)){
		(*loadAdditionalData)();
	}

	glDrawElements(mode, indexCount, GL_UNSIGNED_INT, indices);
	glUseProgram(0);
	glBindVertexArray(0);



	glfwSwapBuffers(window);
	glfwPollEvents();

}
void VIBuffer::DrawArrays(GLFWwindow* window, ShaderProgram& shader, GLenum mode, GLuint size, void(*loadAdditionalData)()){
	glBindVertexArray(VAO);
	glUseProgram(shader.getProgram());
	if ((loadAdditionalData != nullptr)){
		(*loadAdditionalData)();
	}
	glDrawArrays(mode, 0, size);
	glUseProgram(0);
	glBindVertexArray(0);
}