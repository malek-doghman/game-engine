#pragma once
#include "Common.h"


class ShaderProgram;

struct shader_t{
	GLenum shaderType;
	string fileName;
};

class Material{
public:
	Material();
	Material(string name);
	Material(const Material& m);

	vec3 getAmbientColor() const;
	void setAmbientColor(vec3);

	vec3 getDiffuseColor() const;
	void setDiffuseColor(vec3);

	GLfloat getSpecular() const;

	void setSpecular(GLfloat);

	string getMaterialName() const;
	std::set<pair<const Texture*, TextureType>> getTextures() const;

	void bindTextures(ShaderProgram* shader)const;


	void addTexture(const Texture*, TextureType);

private:
	GLfloat specular;
	vec3 ambientColor, diffuseColor;
	std::set<pair<const Texture *, TextureType>> textureFiles;
	string materialName;
};

enum lightType{
	DIRECTIONAL_LIGHT,
	POINT_LIGHT,
	SPOT_LIGHT
};



struct LightAttribute{
	vec3 direction;
	vec3 position;
	vec3 color;
	vec3 eyePos;
	float intensity;
	float radius;
	lightType type;


	LightAttribute();

	LightAttribute& operator=(const LightAttribute& l);

};



class ShaderProgram{
public:
	ShaderProgram();
	void InsertShader(GLenum shaderType, string fileName);
	void BuildProgram();
	GLuint getProgram() const;
	void sendUniformData(string, GLfloat);
	void sendUniformData(string, GLdouble);
	void sendUniformData(string, GLint);
	void sendUniformData(string uniformName, GLuint value);

	void sendUniformData(string, vec2&);
	void sendUniformData(string, vec3&);
	void sendUniformData(string, vec4&);

	void sendUniformData(string, mat3&);
	void sendUniformData(string, mat4&);
	GLint findLocation(string);

	virtual void updateAllUniforms(const Material& m);
	virtual void updateAttributes(const LightAttribute& l);

	~ShaderProgram();

private:
	GLint findShader();

protected:
	vector<shader_t> shaderList;
	vector<GLuint>compiledShaders;
	map<string, GLint> uniformLocations;

	static map<pair<string, string>, GLint> allShaders;

	GLuint builtProgram;
	string readFile(string fileName_);
	void CompileAllShaders();


};



/******************************************************
VIBuffer:
Holds model's GPU related objects(VAO,vertexBuffer,...)
*******************************************************/
struct VIBuffer{
	GLuint VAO;
	GLuint vertexBuffer;
	GLuint normalsBuffer;
	GLuint textureCoordsBuffer;
	GLuint indexBuffer;
	GLuint indexCount;
	VIBuffer(){
		VAO = indexCount = vertexBuffer = indexBuffer = normalsBuffer = textureCoordsBuffer = 0;
	}

	bool isEmpty(){
		return(VAO == 0);
	}

	void DrawElements(GLFWwindow* window, ShaderProgram& shader, GLenum mode, GLvoid* indices, void(*loadAdditionalData)() = nullptr);
	void DrawArrays(GLFWwindow* window, ShaderProgram& shader, GLenum mode, GLuint size, void(*loadAdditionalData)() = nullptr);

};


