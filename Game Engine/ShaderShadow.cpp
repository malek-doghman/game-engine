#include "ShaderShadow.h"


ShaderShadow::ShaderShadow() :ShaderProgram(){
	InsertShader(GL_VERTEX_SHADER, "data/shader/shadow.vert");
	InsertShader(GL_FRAGMENT_SHADER, "data/shader/shadow.frag");
	BuildProgram();

}


void ShaderShadow::updateAllUniforms(const Material& m){
	m.bindTextures(this);
}