#pragma once
#include "ShaderProgram.h"

class ShaderShadow : public ShaderProgram{
public:
	ShaderShadow();
	void updateAllUniforms(const Material& m);
};