#include "ShadowFrameBuffer.h"
#include "WindowGL.h"
#include "ShaderProgram.h"
#include "Utility.h"
#include "FrameBuffer.h"
#include "CompleteFrameBuffer.h"
#include "ColorFrameBuffer.h"


#define SHADOW_RESOLUTION_MULTIPLIER 2

FrameBuffer* ShadowFrameBuffer::shadowDepthsFB = nullptr;
FrameBuffer* ShadowFrameBuffer::shadowColorFilterXFB = nullptr;
FrameBuffer* ShadowFrameBuffer::shadowColorFilterYFB = nullptr;
bool ShadowFrameBuffer::shadowPass = false;


/**********************************************************
Initialize():
Initialize 3 framebuffers:
CompleteFrameBuffer:To store depth values and depth^2
ColorFrameBuffer:Filter depths along the X Axis
ColorFrameBuffer:Filter depths along the Y Axis
***********************************************************/
void ShadowFrameBuffer::Initialize(){
	shadowDepthsFB = new CompleteFrameBuffer(SHADOW_RESOLUTION_MULTIPLIER * WindowGL::GetWidth(), SHADOW_RESOLUTION_MULTIPLIER * WindowGL::GetHeight());
	shadowColorFilterXFB = new ColorFrameBuffer(SHADOW_RESOLUTION_MULTIPLIER * WindowGL::GetWidth(), SHADOW_RESOLUTION_MULTIPLIER * WindowGL::GetHeight(), "data/shader/blurX.frag");
	shadowColorFilterYFB = new ColorFrameBuffer(SHADOW_RESOLUTION_MULTIPLIER * WindowGL::GetWidth(), SHADOW_RESOLUTION_MULTIPLIER * WindowGL::GetHeight(), "data/shader/blurY.frag");
}

void ShadowFrameBuffer::bindWriteFB(int x, int y){
	int width = SHADOW_RESOLUTION_MULTIPLIER * WindowGL::GetWidth();
	width = width / 2;
	int height = SHADOW_RESOLUTION_MULTIPLIER * WindowGL::GetHeight();
	height = height / 2;
	shadowDepthsFB->UpdateFrameBuffer(x* width, y * height, width, height);

	shadowPass = true;
}

void ShadowFrameBuffer::bindWriteFB(){
	shadowDepthsFB->UpdateFrameBuffer();

	shadowPass = true;
}

void ShadowFrameBuffer::unbindWriteFB(){
	shadowDepthsFB->unBindFrameBuffer();
	shadowColorFilterXFB->unBindFrameBuffer();
	shadowColorFilterYFB->unBindFrameBuffer();
	glViewport(0, 0, WindowGL::GetWidth(), WindowGL::GetHeight());

	shadowPass = false;
}

/**********************************************************
bindReadFB():
Binds the final filtered texture of the depth map
***********************************************************/
void ShadowFrameBuffer::bindReadFB(GLuint textureUnit){
	if (shadowColorFilterYFB->checkFrameBufferStatus() == FB_COMPLETE){
		glActiveTexture(GL_TEXTURE0 + textureUnit);
		glBindTexture(GL_TEXTURE_2D, shadowColorFilterYFB->getColorTexture());
		//glGenerateMipmap(GL_TEXTURE_2D);		//Generate MipMap only when the shadow map is changed !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	}
}


/**********************************************************
Draw():
Draws the initial depth framebuffer
***********************************************************/
void ShadowFrameBuffer::Draw(){
	shadowDepthsFB->DrawFrameBuffer();
}

/**********************************************************
applyFilter():
Takes the depth texture and a apply box blur filter
on two seperate passes
***********************************************************/
void ShadowFrameBuffer::applyFilter(){
	glm::vec4 white = vec4(10.0);

	shadowDepthsFB->unBindFrameBuffer();
	shadowColorFilterXFB->unBindFrameBuffer();
	shadowColorFilterYFB->unBindFrameBuffer();

	shadowColorFilterXFB->UpdateFrameBuffer();
	glClearBufferfv(GL_COLOR, 0, &white[0]);
	glClearBufferfv(GL_DEPTH, 0, &white[0]);
	const ShaderProgram& shaderX = shadowColorFilterXFB->getFilter();
	shadowDepthsFB->DrawFrameBuffer(&shaderX);
	shadowColorFilterXFB->unBindFrameBuffer();

	shadowColorFilterYFB->UpdateFrameBuffer();

	glClearBufferfv(GL_COLOR, 0, &white[0]);
	glClearBufferfv(GL_DEPTH, 0, &white[0]);

	const ShaderProgram& shaderY = shadowColorFilterYFB->getFilter();
	shadowColorFilterXFB->DrawFrameBuffer(&shaderY);
	shadowColorFilterYFB->unBindFrameBuffer();
}


vec2 ShadowFrameBuffer::getShadowResolution(){
	return(vec2(SHADOW_RESOLUTION_MULTIPLIER * WindowGL::GetWidth(), SHADOW_RESOLUTION_MULTIPLIER * WindowGL::GetHeight()));
}

bool ShadowFrameBuffer::isShadowPass(){
	return(shadowPass);
}