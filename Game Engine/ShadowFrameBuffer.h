#pragma once
#include "Common.h"

class ShaderProgram;
class FrameBuffer;

class ShadowFrameBuffer{
public:
	static void Initialize();
	static void bindReadFB(GLuint textureUnit);
	static void bindWriteFB(int x, int y);
	static void bindWriteFB();
	static void unbindWriteFB();
	static void Draw();

	static void applyFilter();
	static vec2 getShadowResolution();

	static bool isShadowPass();

private:
	static FrameBuffer* shadowDepthsFB;
	static FrameBuffer* shadowColorFilterXFB;
	static FrameBuffer* shadowColorFilterYFB;

	static bool shadowPass;
};