#include "SplashScreen.h"
#include "Utility.h"
#include "ShaderProgram.h"
#include "Textures.h"
#include "WindowGL.h"

bool SplashScreen::show = false;
bool SplashScreen::isLoaded = false;
VIBuffer SplashScreen::bufferSpecs;
ShaderProgram* SplashScreen::shader = nullptr;
GLFWwindow* SplashScreen::offScreenContext = nullptr;
GLuint SplashScreen::splashTexture = 0;

void SplashScreen::loadSplashScreen(string fileName){
	glfwMakeContextCurrent(WindowGL::GetWindowPointer());
	if (glewInit() == 0){


		bufferSpecs = Utility::getRectangle();

		shader = new ShaderProgram();

		shader->InsertShader(GL_VERTEX_SHADER, "data/shader/screen.vert");
		shader->InsertShader(GL_FRAGMENT_SHADER, "data/shader/splash.frag");
		shader->BuildProgram();

		splashTexture = (Textures::addTexture(fileName, TextureType::DIFFUSE_TEXTURE));


		isLoaded = true;
	}
}


void SplashScreen::startSplashScreen(string s){
	if (!isLoaded){
		loadSplashScreen(s);
	}

	show = true;

	while (show){
		static GLFWwindow* window = WindowGL::GetWindowPointer();
		glfwMakeContextCurrent(window);
		bufferSpecs.DrawElements(window, *shader, GL_TRIANGLES, 0, ClearAndBindTextures);
	}
}


void SplashScreen::stopSplashScreen(){
	show = false;
}


void SplashScreen::ClearAndBindTextures(){
	static vec3 backGColor = vec3(0.0, 0.0, 0.0);
	static float one = 1.0f;
	glActiveTexture(GL_TEXTURE1);
	glClearBufferfv(GL_COLOR, 0, &backGColor[0]);
	glClearBufferfv(GL_DEPTH, 0, &one);
	glBindTexture(GL_TEXTURE_2D, splashTexture);

	shader->sendUniformData("time", glfwGetTime());

}