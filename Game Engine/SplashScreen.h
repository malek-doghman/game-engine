#pragma once

#include "Common.h"
#include "ShaderProgram.h"

class SplashScreen{
public:
	static void loadSplashScreen(string fileName = "data/texture/splash.png");
	static void startSplashScreen(string fileName = "data/texture/splash.png");
	static void stopSplashScreen();

private:
	static void ClearAndBindTextures();
	static bool show;
	static bool isLoaded;
	static VIBuffer bufferSpecs;
	static ShaderProgram* shader;
	static GLuint splashTexture;
	static GLFWwindow* offScreenContext;
};