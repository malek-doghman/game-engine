#include "SpotLight.h"
#include "WindowGL.h"
#include "LightModel.h"


SpotLight::SpotLight() :Light(){
	lAttributes.type = SPOT_LIGHT;
	Initialize();
	lightModel = new LightModel("spotLight.obj");
}

void SpotLight::Initialize(){
	InitializePerspectiveMatrix();
}

void SpotLight::Update(){
	Light::Update();
	UpdateVPMatrix();
}

void SpotLight::InitializePerspectiveMatrix(){
	perspectiveMatrix = glm::perspective(180.0f / WindowGL::GetAspectRatio(), WindowGL::GetAspectRatio(), 50.1f, 1000.0f);

}


void SpotLight::UpdateVPMatrix(){
	VPMatrix = perspectiveMatrix * viewMatrix;
}

mat4 SpotLight::getVPMatrix(){
	return(VPMatrix);
}

mat4 SpotLight::getVPByIndex(unsigned int i){
	return(getVPMatrix());
}
