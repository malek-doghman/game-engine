#pragma once
#include "Light.h"

class SpotLight : public Light{
public:
	SpotLight();
	virtual void Initialize() override;
	virtual void Update() override;
	virtual mat4 getVPByIndex(unsigned int i) override;
	virtual mat4 getVPMatrix() override;

private:
	void UpdateVPMatrix();
	void InitializePerspectiveMatrix();
	mat4 perspectiveMatrix;
	mat4 VPMatrix;

};