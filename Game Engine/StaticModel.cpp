#include "StaticModel.h"
#include "ShaderProgram.h"
#include "Textures.h"
#include "Mesh.h"
#include "Camera.h"
#include "ShadowFrameBuffer.h"
#include "Light.h"
#include "Utility.h"

StaticModel::StaticModel(string _fileName) :Model(_fileName), InputController(true, true)
{
	fillDataFromMeshes();
	clearMeshes();
}


/**********************************************************
Initialize():
Initialzing all GPU buffers and copy data from RAM to VRAM.
RAM Data is cleared as it will never be used again
by the client.
***********************************************************/
void StaticModel::Initialize(){


	//updating boundries
	boundingBox.Update();

	//Computer a translation-matrix to origin 
	computeCenterMatrix(boundingBox.getBounds());

	glGenBuffers(1, &bufferSpecs.vertexBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, bufferSpecs.vertexBuffer);
	glBufferData(GL_ARRAY_BUFFER, Vertices.size() * sizeof(glm::vec3), &Vertices[0], GL_STATIC_DRAW);

	glGenBuffers(1, &bufferSpecs.indexBuffer);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, bufferSpecs.indexBuffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, Indices.size() * sizeof(unsigned int), &Indices[0], GL_STATIC_DRAW);

	bufferSpecs.indexCount = Indices.size();

	glGenVertexArrays(1, &bufferSpecs.VAO);
	glBindVertexArray(bufferSpecs.VAO);

	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(vec3), 0);


	glGenBuffers(1, &bufferSpecs.normalsBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, bufferSpecs.normalsBuffer);
	glBufferData(GL_ARRAY_BUFFER, Normals.size()* sizeof(vec3), &Normals[0], GL_STATIC_DRAW);

	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, sizeof(vec3), 0);


	glGenBuffers(1, &bufferSpecs.textureCoordsBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, bufferSpecs.textureCoordsBuffer);

	if (uvChannels[0]->size() > 0){
		vector<vec2>* v = uvChannels[0];
		glBufferData(GL_ARRAY_BUFFER, v->size()* sizeof(vec2), &v->at(0), GL_STATIC_DRAW);

		glEnableVertexAttribArray(1);
		glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, 0);


		glBindVertexArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	}

	ClearRAM();
}

void StaticModel::InitializeDirection(const mat4& rotateMatrix){
	transformationMatrix = transformationMatrix * rotateMatrix;
	updateWorldNormalMatrix(transformationMatrix);
}


void StaticModel::InitializePosition(const mat4& translateMatrix){
	transformationMatrix = translateMatrix * transformationMatrix;
	updateWorldNormalMatrix(transformationMatrix);
}

void StaticModel::updateWorldNormalMatrix(const mat4& transform){
	worldMatrix = toPositionMatrix*transform * toCenterMatrix;
	normalMatrix = mat3(glm::transpose(glm::inverse(worldMatrix)));
}

void StaticModel::Update(){
	boundingBox.isIntersected();
	selected = boundingBox.isSelected();
	if ((selected) && (getTransformation(transformationMatrix))){
		boundingBox.Update(transformationMatrix);
		updateWorldNormalMatrix(transformationMatrix);
	}
}




/**********************************************************
Draw():
Draws the whole static model
Depth texture is bound to to texture unit 1 !
***********************************************************/
void StaticModel::Draw(ShaderProgram& shader, Light* l){
	glDisable(GL_CULL_FACE);
	glBindVertexArray(bufferSpecs.VAO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, bufferSpecs.indexBuffer);

	glUseProgram(shader.getProgram());
	shader.updateAllUniforms(mat);


	mat4 matrix = Camera::getViewProjectionMatrix() * worldMatrix;

	shader.sendUniformData("WVPMatrix", matrix);
	shader.sendUniformData("WorldMatrix", worldMatrix);
	shader.sendUniformData("NormalMatrix", normalMatrix);
	shader.sendUniformData("ViewMatrix", Camera::getViewMatrix());

	shader.sendUniformData("instanced", 0);

	if (l != nullptr){
		shader.sendUniformData("VPMatrixLight", l->getVPByIndex(0));
		shader.sendUniformData("VPL1", l->getVPByIndex(0));
		shader.sendUniformData("VPL2", l->getVPByIndex(1));
		shader.sendUniformData("VPL3", l->getVPByIndex(2));
		shader.sendUniformData("VPL4", l->getVPByIndex(3));
	}

	glDrawElements(GL_TRIANGLES, bufferSpecs.indexCount, GL_UNSIGNED_INT, 0);

	glBindVertexArray(0);
	glUseProgram(0);

	if (selected){
		boundingBox.Draw();
	}

	glEnable(GL_CULL_FACE);

}

void StaticModel::addVertexAttribute(GLuint index, GLint size, GLenum type, GLboolean normalized, GLsizei stride, const GLvoid *pointer, GLuint divisorCount){
	glBindVertexArray(bufferSpecs.VAO);
	glVertexAttribPointer(index, size, type, normalized, stride, pointer);
	glEnableVertexAttribArray(index);

	if (divisorCount > 0){
		glVertexAttribDivisor(index, divisorCount);
	}

	glBindVertexArray(0);
}

void StaticModel::DrawInstanced(GLsizei instanceCount, ShaderProgram& shader, Light* l){
	glDisable(GL_CULL_FACE);
	glBindVertexArray(bufferSpecs.VAO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, bufferSpecs.indexBuffer);


	glUseProgram(shader.getProgram());
	shader.updateAllUniforms(mat);


	mat4 matrix = Camera::getViewProjectionMatrix()  * worldMatrix;


	shader.sendUniformData("WVPMatrix", matrix);
	shader.sendUniformData("WorldMatrix", matrix);
	shader.sendUniformData("NormalMatrix", matrix);
	shader.sendUniformData("ViewMatrix", Camera::getViewMatrix());

	shader.sendUniformData("instanced", 1);

	if (l != nullptr){
		shader.sendUniformData("VPMatrixLight", l->getVPByIndex(0));
		shader.sendUniformData("VPL1", l->getVPByIndex(0));
		shader.sendUniformData("VPL2", l->getVPByIndex(1));
		shader.sendUniformData("VPL3", l->getVPByIndex(2));
		shader.sendUniformData("VPL4", l->getVPByIndex(3));
	}

	glDrawElementsInstanced(GL_TRIANGLES, bufferSpecs.indexCount, GL_UNSIGNED_INT, 0, instanceCount);

	glBindVertexArray(0);
	glUseProgram(0);

	if (selected){
		boundingBox.Draw();
	}
	glEnable(GL_CULL_FACE);
}




/**********************************************************
fillDataFromMeshes():
Merges all vertex data from meshes in one big container.
***********************************************************/
void StaticModel::fillDataFromMeshes(){

	if (meshes.size() > 0){
		mat = meshes[0]->getMaterial();
		vector < glm::vec2 >* v = new vector < glm::vec2 >;
		uvChannels.push_back(v);

		for (Mesh* m : meshes){

			unsigned int size = Vertices.size();
			Vertices.insert(Vertices.end(), m->getVertices().begin(), m->getVertices().end());

			boundingBox.updateBounds(m->getBoundingBox().getBounds().first);
			boundingBox.updateBounds(m->getBoundingBox().getBounds().second);


			vector<unsigned int>& ind = m->getIndices();
			for (unsigned int i = 0; i < ind.size(); i++){
				Indices.push_back(ind[i] + size);
			}

			if (m->getUVChannels().size() > 0){
				uvChannels[0]->insert(uvChannels[0]->end(), m->getUVChannels()[0]->begin(), m->getUVChannels()[0]->end());
			}

			Normals.insert(Normals.end(), m->getNormals().begin(), m->getNormals().end());
			Tangents.insert(Tangents.begin(), m->getTangents().begin(), m->getTangents().end());
			biNormals.insert(biNormals.begin(), m->getBiNormals().begin(), m->getBiNormals().end());
		}
	}

	boundingBox.Update();
}


VIBuffer StaticModel::getVertexIndexBuffer(){
	return(bufferSpecs);
}

StaticModel::~StaticModel(){
	ClearVRAM();
	ClearTextures();
}

void StaticModel::ClearRAM(){
	Vertices.clear();
	Normals.clear();
	biNormals.clear();
	Tangents.clear();
	Indices.clear();
	for (vector<glm::vec2>* v : uvChannels){
		v->clear();
	}
	uvChannels.clear();


}

void StaticModel::ClearVRAM(){
	glDeleteBuffers(1, &bufferSpecs.vertexBuffer);
	glDeleteBuffers(1, &bufferSpecs.indexBuffer);
	glDeleteBuffers(1, &bufferSpecs.normalsBuffer);
	glDeleteBuffers(1, &bufferSpecs.textureCoordsBuffer);

	glDeleteVertexArrays(1, &bufferSpecs.VAO);

}

void StaticModel::ClearTextures(){
	for (pair<GLuint, TextureType> t : textureFiles){
		glDeleteTextures(1, &t.first);
	}

	textureFiles.clear();
}


void StaticModel::computeCenterMatrix(const pair<vec3, vec3>&_minMax){
	vec3 centerPos = (_minMax.first + _minMax.second) / 2.0f;
	toCenterMatrix = glm::translate(mat4(), -centerPos);
	toPositionMatrix = glm::translate(mat4(), centerPos);
}
