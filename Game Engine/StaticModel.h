#pragma once

#include "Common.h"
#include "Model.h"
#include "BoundingBox.h"
#include "InputController.h"


class StaticModel : public Model, public InputController{
public:
	StaticModel(string fileName_);
	virtual void Initialize() override;
	void InitializeDirection(const mat4& rotateMatrix);
	void InitializePosition(const mat4& translateMatrix);
	virtual void Update() override;
	void Update(mat4 _WorldMatrix);
	virtual void Draw(ShaderProgram& shader, Light* l = nullptr) override;
	void DrawInstanced(GLsizei instanceCount, ShaderProgram& shader, Light* l = nullptr);
	void addVertexAttribute(GLuint index, GLint size, GLenum type, GLboolean normalized, GLsizei stride, const GLvoid *pointer, GLuint divisorCount = 0);
	VIBuffer getVertexIndexBuffer() override;
	bool isSelected();
	void updateWorldNormalMatrix(const mat4& translateMatrix);
	~StaticModel();

protected:
	void fillDataFromMeshes();

	void ClearRAM();
	void ClearVRAM();
	void ClearTextures();

	void computeCenterMatrix(const pair<vec3, vec3>&_minMax);

	mat4 worldMatrix,
		toCenterMatrix,
		toPositionMatrix,
		transformationMatrix;
	mat3 normalMatrix;

	vector<glm::vec3>Vertices;
	vector<glm::vec3>Normals;
	vector<glm::vec3>biNormals;
	vector<glm::vec3>Tangents;
	vector<unsigned int>Indices;
	vector<vector<glm::vec2>*> uvChannels;

	VIBuffer bufferSpecs;
	GLuint textureBuffer;

	std::set<pair<GLuint, TextureType>> textureFiles;

	BoundingBox boundingBox;
	bool selected;
	Material mat;

};