#include "Terrain.h"
#include "Utility.h"
#include "glException.h"
#include "Camera.h"
#include "Light.h"
#include "ShadowFrameBuffer.h"
#include "Textures.h"

const unsigned int TRIANGLES_PER_FACE = 2;
const unsigned int VERTICES_PER_TRIANGLE = 3;

Terrain::Terrain(string _heightMap, float _Scale, unsigned int _Res) :GameComponent(true, true),
Resolution(_Res), Scale(_Scale)
{
	unsigned char* heightMap = SOIL_load_image(_heightMap.c_str(), &mapWidth, &mapHeight, &mapChannels, SOIL_LOAD_RGB);
	Colors = new RGB*[mapHeight];
	for (unsigned int i = 0; i < mapHeight; i++){
		Colors[i] = new RGB[mapWidth];
		memcpy(Colors[i], heightMap + i*mapWidth*sizeof(RGB), mapWidth*sizeof(RGB));
	}
	delete[] heightMap;
	InitializePlane();
	colorTexture = const_cast<Texture*>(Textures::addTextureIntoRAM("data/texture/grass.jpg", TextureType::DIFFUSE_TEXTURE));
}


void Terrain::Initialize(){

	uploadToGPU();

	GLuint textureID = Textures::loadTextureIntoVRAM(colorTexture);
	textureFiles.insert(pair<GLuint, TextureType >(textureID, TextureType::DIFFUSE_TEXTURE));
	glBindTexture(GL_TEXTURE_2D, textureID);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	Utility::enableAnisotropicFiltering();
	glBindTexture(GL_TEXTURE_2D, 0);
}


void Terrain::Update(){
}

void Terrain::Draw(ShaderProgram& shader, Light* l){
	if (ShadowFrameBuffer::isShadowPass()){
		Utility::switchFrontFaceMode(GL_CCW);
	}


	glBindVertexArray(VAO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, PlaneIBO);
	glUseProgram(shader.getProgram());
	shader.updateAllUniforms(mat);


	mat4 matrix = Camera::getViewProjectionMatrix() * worldMatrix;

	shader.sendUniformData("WVPMatrix", matrix);
	shader.sendUniformData("WorldMatrix", worldMatrix);
	shader.sendUniformData("NormalMatrix", normalMatrix);
	shader.sendUniformData("ViewMatrix", Camera::getViewMatrix());

	shader.sendUniformData("instanced", 0);

	if (l != nullptr){
		shader.sendUniformData("VPMatrixLight", l->getVPByIndex(0));
		shader.sendUniformData("VPL1", l->getVPByIndex(0));
		shader.sendUniformData("VPL2", l->getVPByIndex(1));
		shader.sendUniformData("VPL3", l->getVPByIndex(2));
		shader.sendUniformData("VPL4", l->getVPByIndex(3));
	}


	Utility::bindTextures(shader, textureFiles);

	glDrawElements(GL_TRIANGLES, indicesSize, GL_UNSIGNED_INT, 0);

	glBindVertexArray(0);
	glUseProgram(0);

	if (ShadowFrameBuffer::isShadowPass()){
		Utility::switchFrontFaceMode(GL_CW);
	}
}

void Terrain::InitializePlane(){
	if ((Resolution % 10) != 0){
		throw(glException("Bad Resolution for terrain !"));
	}
	if (Resolution > std::min(mapWidth, mapHeight)){
		Resolution = std::min(mapWidth, mapHeight);
	}

	float xyStep = 1.0f / Resolution;


	indicesSize = Resolution * Resolution  * TRIANGLES_PER_FACE * VERTICES_PER_TRIANGLE;
	indices = new GLuint[indicesSize];


	vertexCount = (Resolution + 1) * (Resolution + 1);
	vertices = new vPosTCoords[vertexCount];
	unsigned int index = 0;
	vertexCount = 0;
	int textureStep = (float)std::min(mapWidth, mapHeight) / (Resolution);
	int di = 0;
	for (float i = 0.0; i <= 1.0; i += xyStep){
		int dj = 0;
		for (float j = 0.0; j <= 1.0; j += xyStep){
			//Vertex:
			vertices[index].vPos.x = j *Scale;
			vertices[index].vPos.y = getHeight(di, dj);
			vertices[index].vPos.z = -i *Scale;

			//Texture Coords:
			vertices[index].tCoords.x = 50 * j;
			vertices[index++].tCoords.y = 50 * i;

			vertexCount++;
			dj++;
		}
		di++;
	}

	index = 0;
	for (unsigned int i = 0; i < Resolution; i++){
		for (unsigned int j = 0; j < Resolution; j++){

			//1st Triangle for (i,j) face
			indices[index++] = getPointIndex(i, j);
			indices[index++] = getPointIndex(i, j + 1);
			indices[index++] = getPointIndex(i + 1, j);

			//Normals for 1st Triangle
			vec3 v1 = vertices[indices[index - 2]].vPos - vertices[indices[index - 3]].vPos;
			vec3 v2 = vertices[indices[index - 1]].vPos - vertices[indices[index - 3]].vPos;
			vec3 n = glm::cross(v1, v2);
			vertices[getPointIndex(i, j)].vNorm = n;
			vertices[getPointIndex(i, j + 1)].vNorm = n;
			vertices[getPointIndex(i + 1, j)].vNorm = n;

			//2nd Triangle for (i,j) face
			indices[index++] = getPointIndex(i, j + 1);
			indices[index++] = getPointIndex(i + 1, j + 1);
			indices[index++] = getPointIndex(i + 1, j);


			v1 = vertices[indices[index - 2]].vPos - vertices[indices[index - 3]].vPos;
			v2 = vertices[indices[index - 1]].vPos - vertices[indices[index - 3]].vPos;
			n = glm::cross(v1, v2);
			vertices[getPointIndex(i + 1, j + 1)].vNorm = n;
		}
	}
}


void Terrain::uploadToGPU(){
	glGenBuffers(1, &PlaneVBO);
	glBindBuffer(GL_ARRAY_BUFFER, PlaneVBO);
	glBufferData(GL_ARRAY_BUFFER, vertexCount*sizeof(vPosTCoords), vertices, GL_STATIC_DRAW);

	glGenBuffers(1, &PlaneIBO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, PlaneIBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(GLuint) * indicesSize, indices, GL_STATIC_DRAW);

	glGenVertexArrays(1, &VAO);
	glBindVertexArray(VAO);

	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(vPosTCoords), (void*)(offsetof(vPosTCoords, vPos)));

	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(vPosTCoords), (void*)(offsetof(vPosTCoords, tCoords)));

	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, sizeof(vPosTCoords), (void*)(offsetof(vPosTCoords, vNorm)));

	glBindVertexArray(0);

	delete[] indices;
}

int Terrain::getPointIndex(int i, int j){
	return(i*(Resolution + 1) + j);
}

float Terrain::getHeight(int di, int dj){
	float textureStep = (float)std::min(mapWidth, mapHeight) / (Resolution);
	float y = (float)(di)*textureStep;
	float x = (float)(dj)*textureStep;

	int yM = floor(y);
	float yL = y - yM;

	int xM = floor(x);
	float xL = x - xM;

	float h1 = ((Colors[std::min(yM, mapHeight - 1)][std::min(xM, mapWidth - 1)].R)*(1 - xL) + ((Colors[std::min(yM, mapHeight - 1)][std::min(xM + 1, mapWidth - 1)].R)*(xL)))*(1 - yL);
	float h2 = (((Colors[std::min(yM + 1, mapHeight - 1)][std::min(xM + 1, mapWidth - 1)].R)*(xL)) + ((Colors[std::min(yM + 1, mapHeight - 1)][std::min(xM, mapWidth - 1)].R)*(1 - xL)))*yL;

	return(h1 + h2);
}

float Terrain::getHeightAt(const vec3& pos){
	float height = 0;
	if ((pos.x > 0) && (pos.x < Scale) && (-pos.z>0) && (-pos.z < Scale)){
		float r = (Resolution + 1) / Scale;
		float z = pos.x *r;
		float x = -pos.z *r;
		int xM = std::floor(x);
		float xL = x - xM;



		int zM = std::floor(z);
		float zL = z - zM;

		float h = 0;
		int dist = 3;
		int sq = (2 * dist) + 1;
		sq = sq*sq;
		for (int i = -dist; i <= dist; i++){
			for (int j = -dist; j <= dist; j++){
				int X = (x + i) >= 0 ? (x + i) : 0;
				X = X < Resolution ? X : (Resolution - 1);

				int Z = (z + j) >= 0 ? z + j : 0;
				Z = Z < Resolution ? Z : (Resolution - 1);

				h += getHeight(X, Z);
			}
		}
		height = h / sq;
	}
	return(height);
}

vec3 Terrain::getNormal(int di, int dj){
	int index = getPointIndex(di, dj);
	return(vertices[index].vNorm);
}


vec3 Terrain::getNormalAt(const vec3& pos){
	vec3 height = vec3(0);
	if ((pos.x > 0) && (pos.x < Scale) && (-pos.z>0) && (-pos.z < Scale)){
		float r = (Resolution + 1) / Scale;
		float z = pos.x *r;
		float x = -pos.z *r;
		int xM = std::floor(x);
		float xL = x - xM;

		int zM = std::floor(z);
		float zL = z - zM;

		vec3 h = vec3(0);
		int dist = 3;
		float sq = (2 * dist) + 1;
		sq = sq*sq;
		for (int i = -dist; i <= dist; i++){
			for (int j = -dist; j <= dist; j++){

				int X = (x + i) >= 0 ? (x + i) : 0;
				X = X < Resolution ? X : (Resolution - 1);

				int Z = (z + j) >= 0 ? z + j : 0;
				Z = Z < Resolution ? Z : (Resolution - 1);

				h += getNormal(X, Z);
			}
		}
		height = h / sq;
	}
	return(height);
}




float Terrain::getScale(){
	return(Scale);
}

float Terrain::getSurface(){
	return(Scale * Scale);
}