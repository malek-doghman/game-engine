#pragma once
#include "Common.h"
#include "GameComponent.h"
#include "ShaderProgram.h"

struct vPosTCoords{
	glm::vec3 vPos;
	glm::vec3 vNorm;
	glm::vec2 tCoords;
};

struct RGB{
	unsigned char R;
	unsigned char G;
	unsigned char B;
};

class Terrain : public GameComponent{
public:
	Terrain(string heightMap, float _Scale, unsigned int _Res);
	virtual void Initialize() override;
	virtual void Update() override;
	virtual void Draw(ShaderProgram& shader, Light* l = nullptr) override;
	float getHeightAt(const vec3& pos);
	vec3 getNormalAt(const vec3& pos);
	void populateWithObjects(unsigned int objCount);
	float getScale();
	float getSurface();
	void uploadToGPU();

protected:
	unsigned int Resolution;
	float Scale;
	float getHeight(int di, int dj);
	vec3 getNormal(int di, int dj);

	int mapWidth, mapHeight, mapChannels;

	GLuint heightMapTexture;
	GLuint VAO;
	GLuint PlaneVBO;
	GLuint PlaneIBO;
	RGB** Colors;
	unsigned int indicesSize;
	mat4 worldMatrix;
	mat3 normalMatrix;
private:
	void InitializePlane();
	int getPointIndex(int i, int j);
	std::set<pair<GLuint, TextureType>> textureFiles;
	vPosTCoords* vertices;
	GLuint vertexCount;
	Material mat;
	GLuint* indices;
	Texture* colorTexture;
};