#include "Textures.h"
#include "Utility.h"
#include "glException.h"

map<string, Texture> Textures::AllTextures;
vector< Texture*> Textures::unloadedGPU;



/**********************************************************
addTexture():
Adds a texture (if not found) in the vector AllTextures
***********************************************************/
GLuint Textures::addTexture(string name, TextureType type){
	GLuint id = 0;
	map<string, Texture>::iterator Iterator = AllTextures.find(name);
	unsigned int loadType = SOIL_LOAD_RGB;

	switch (type){
	case ALPHA_TEXTURE:
		loadType = SOIL_LOAD_L;
		break;
		//TODO - Other texture types
	default:
		break;
	}


	if (Iterator == AllTextures.end()){
		unsigned int flags = SOIL_FLAG_INVERT_Y | SOIL_FLAG_NTSC_SAFE_RGB | SOIL_FLAG_MIPMAPS;
		string textureName = name;
		std::cout << textureName.c_str() << std::endl;
		id = SOIL_load_OGL_texture(textureName.c_str(), loadType, SOIL_CREATE_NEW_ID, flags);
		pair<string, Texture> p;
		p.first = name;
		Texture t;
		t.id = id;
		t.type = type;
		t.name = name;
		p.second = t;
		AllTextures.insert(p);
	}
	else{
		id = Iterator->second.id;
	}
	return(id);
}


const Texture* Textures::addTextureIntoRAM(string name, TextureType type){
	Texture* tex = loadTextureIntoRAM(name, type);
	unloadedGPU.push_back(tex);
	return(tex);
}


Texture* Textures::loadTextureIntoRAM(string _name, TextureType type){
	Texture* t = new Texture();
	t->type = type;
	t->name = _name;
	t->rawData = SOIL_load_image(_name.c_str(), &t->width, &t->height, &t->mapChannels, SOIL_LOAD_AUTO);
	std::cout << _name << "\t----> OK" << std::endl;
	return(t);
}



GLuint Textures::loadTextureIntoVRAM(Texture* texture){
	if ((texture != nullptr) && (texture->rawData != nullptr)){
		map<string, Texture>::iterator Iterator = AllTextures.find(texture->name);
		if (Iterator == AllTextures.end()){
			unsigned int mips = floor(log2(std::min(texture->width, texture->height)));
			texture->id = Utility::genTextureFromData(texture->rawData, texture->width, texture->height, mips, texture->mapChannels);
			AllTextures.insert(pair<string, Texture>(texture->name, *texture));
		}
		else{
			texture->id = Iterator->second.id;
		}

		delete[] texture->rawData;
		texture->rawData = nullptr;

	}
	if (texture->id <1){
	throw (glException("Texture is not loaded into VRAM or not found !"));
	}
	return(texture->id);
}


void Textures::loadAllTexturesIntoVRAM(){
	for (Texture* t : unloadedGPU){
		t->id = loadTextureIntoVRAM(t);
	}
	unloadedGPU.clear();
}

/**********************************************************
addTexture():
Looks for a texture by its filename
returns its id
***********************************************************/
GLuint Textures::findTexture(string name){
	GLuint textureBuffer = 0;
	map<string, Texture>::iterator Iterator = AllTextures.find(name);
	if (Iterator != AllTextures.end()){
		textureBuffer = Iterator->second.id;
	}
	return(textureBuffer);

}


/**********************************************************
addTexture():
delete a texture (if found) from the vector AllTextures
***********************************************************/
void Textures::deleteTexture(string fileName){
	map<string, Texture>::iterator iterator = AllTextures.find(fileName);

	if (iterator != AllTextures.end()){
		glDeleteTextures(1, &iterator->second.id);
		AllTextures.erase(iterator);
	}

}

GLuint Textures::unloadedTexturesCount(){
	return(unloadedGPU.size());
}