#pragma once
#include "Common.h"

class Textures{

public:
	static GLuint addTexture(string name, TextureType type);	//Add a texture in the container
	static const Texture* addTextureIntoRAM(string name, TextureType type);
	static GLuint findTexture(string name);						//Retrieve texture id from texture filename
	static void deleteTexture(string name);						//Delete A texture from the container
	static Texture* loadTextureIntoRAM(string name, TextureType type);
	static GLuint loadTextureIntoVRAM(Texture* texture);
	static void loadAllTexturesIntoVRAM();
	static GLuint unloadedTexturesCount();

private:
	static map<string, Texture> AllTextures;
	static vector<Texture*> unloadedGPU;
};