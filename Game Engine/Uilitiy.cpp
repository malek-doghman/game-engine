#include <fmod.h>
#include<fmod_errors.h>
#include<fmoddyn.h>
#include "Utility.h"
#include "StaticModel.h"
#include "glException.h"
#include "WindowGL.h"
#include "Textures.h"
#include "ShadowFrameBuffer.h"

map<lightType, VIBuffer> Utility::lightModels;
bool Utility::loaded = false;
GLuint Utility::VAO = 0;
VIBuffer Utility::rectangle;
VIBuffer Utility::cube;
ShaderProgram* Utility::lightShader = nullptr;


bool Utility::blendEnabled = false;
bool Utility::lastBlendingState = false;
GLenum Utility::lastSFactor = GL_ONE;
GLenum Utility::lastDFactor = GL_ONE;
GLenum Utility::currentSFactor = GL_ONE;
GLenum Utility::currentDFactor = GL_ONE;

GLenum Utility::frontFaceMode = GL_CCW;

void Utility::loadModel(lightType type, string fileName){
	pair<lightType, VIBuffer> p;
	Model* model = new StaticModel(fileName);
	model->Initialize();
	p.first = type;

	if (model->getVertexIndexBuffer().indexCount > 0){
		p.second = model->getVertexIndexBuffer();
		lightModels.insert(p);
	}
	else{
		throw(glException("Unable to load light model !"));
	}
}


void Utility::loadLightModels(){
	if (!loaded){
		InitializeShader();
		loadModel(SPOT_LIGHT, "spotLight.obj");
		loadModel(POINT_LIGHT, "pointLight.obj");
		loadModel(DIRECTIONAL_LIGHT, "directionalLight.obj");
		loaded = true;

	}

}

VIBuffer Utility::getLightModel(lightType l){
	if (lightModels.size() < 1){
		loadLightModels();
	}
	map<lightType, VIBuffer>::iterator iterator = lightModels.begin();
	iterator = lightModels.find(l);
	if (iterator == lightModels.end()){
		throw(glException("Light model didn't load !"));
	}
	else{
		return(iterator->second);
	}
}


void Utility::InitializeShader(){
	if (lightShader == nullptr){
		lightShader = new ShaderProgram;
	}
	lightShader->InsertShader(GL_VERTEX_SHADER, "data/shader/lightSprite.vert");
	lightShader->InsertShader(GL_FRAGMENT_SHADER, "data/shader/lightSprite.frag");
	lightShader->BuildProgram();
}

ShaderProgram* Utility::getShader(){
	return(lightShader);
}



void Utility::disableFrameBuffer(){
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glViewport(0, 0, WindowGL::GetWidth(), WindowGL::GetHeight());
}


void Utility::InitializeRectangle(){
	GLfloat vertices[] = {
		1.0, 1.0,
		-1.0, 1.0,
		-1.0, -1.0,
		1.0, -1.0
	};

	GLuint indices[] =
	{
		0, 1, 2,
		3, 0, 2

	};


	glGenVertexArrays(1, &rectangle.VAO);
	glBindVertexArray(rectangle.VAO);

	glGenBuffers(1, &rectangle.vertexBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, rectangle.vertexBuffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), &vertices[0], GL_STATIC_DRAW);

	glGenBuffers(1, &rectangle.indexBuffer);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, rectangle.indexBuffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), &indices[0], GL_STATIC_DRAW);

	rectangle.indexCount = sizeof(indices);

	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(GLfloat) * 2, 0);

	glBindVertexArray(0);
}

void Utility::InitializeCube(){
	GLfloat cubeVertices[] = {
		-0.5, -0.5, 0.5,

		0.5, -0.5, 0.5,

		-0.5, -0.5, -0.5,

		0.5, -0.5, -0.5,

		-0.5, 0.5, 0.5,

		0.5, 0.5, 0.5,

		-0.5, 0.5, -0.5,

		0.5, 0.5, -0.5
	};

	GLuint indices[] = {
		0, 1,
		1, 5,
		5, 4,
		4, 0,

		4, 6,
		6, 7,
		7, 5,

		7, 3,
		3, 2,
		2, 6,

		3, 1,
		2, 0

	};
	cube.indexCount = 24;

	glGenBuffers(1, &cube.vertexBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, cube.vertexBuffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(cubeVertices), cubeVertices, GL_STATIC_DRAW);

	glGenBuffers(1, &cube.indexBuffer);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, cube.indexBuffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

	glGenVertexArrays(1, &cube.VAO);
	glBindVertexArray(cube.VAO);

	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), 0);

	glBindVertexArray(0);

}



VIBuffer Utility::getRectangle(){
	if (rectangle.isEmpty()){
		InitializeRectangle();
	}
	return(rectangle);
}


void Utility::playSound(string fileName,bool repeat){
	static bool soundInit = false;
	if (!soundInit){
		FSOUND_Init(44100, 32, 0);
		soundInit = !soundInit;
	}
	static int index = 0;
	index++;

	FSOUND_SAMPLE* g_sample = FSOUND_Sample_Load(index, fileName.c_str(), 0, 0, 0);
	int ch = FSOUND_PlaySound(FSOUND_FREE, g_sample);
	FSOUND_PlaySound(index, g_sample);

	if (repeat){
	FSOUND_SetLoopMode(index, FSOUND_LOOP_NORMAL);
	}

}

VIBuffer Utility::getCube(){
	if (cube.isEmpty()){
		InitializeCube();
	}
	return(cube);
}

GLuint Utility::genEmptyTexture(unsigned int width, unsigned int height, unsigned int mipLevel, unsigned int pixelFormat){
	GLuint textureBuffer;
	glGenTextures(1, &textureBuffer);
	glBindTexture(GL_TEXTURE_2D, (textureBuffer));
	glTexStorage2D(GL_TEXTURE_2D, mipLevel, pixelFormat, width, height);
	glBindTexture(GL_TEXTURE_2D, 0);
	return(textureBuffer);

}

GLuint Utility::genTextureFromData(unsigned char* _rawData, unsigned int width, unsigned int height, unsigned int mipLevel, GLint channelCount){
	GLuint textureBuffer;
	glGenTextures(1, &textureBuffer);
	glBindTexture(GL_TEXTURE_2D, (textureBuffer));

	GLenum format = GL_RGBA;
	if (channelCount < 4){
		format = GL_RGB;
	}

	glTexStorage2D(GL_TEXTURE_2D, mipLevel, format, width, height);
	glTexImage2D(GL_TEXTURE_2D, 0, format, width, height, 0, format, GL_UNSIGNED_BYTE, _rawData);
	glGenerateMipmap(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, 0);
	return(textureBuffer);
}

void Utility::genVertexBuffer(GLuint* vertexBuffer, unsigned int dataSize, void* data){
	glGenBuffers(1, vertexBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, *vertexBuffer);
	glBufferData(GL_ARRAY_BUFFER, dataSize, data, GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}

bool Utility::isRightHanded(const vec3& _Right, const vec3& _Up, const vec3& _Forward){
	return(glm::normalize(glm::cross(_Right, _Up)) == glm::normalize(_Forward));
}

void Utility::enableBlending(GLenum sFactor, GLenum dFactor){
	if (!blendEnabled){
		glEnable(GL_BLEND);
		if ((currentSFactor != lastSFactor) || (currentSFactor != lastSFactor)){
			lastSFactor = currentSFactor;
			lastDFactor = currentDFactor;
		}
		currentSFactor = sFactor;
		currentDFactor = dFactor;
		glBlendFunc(sFactor, dFactor);
	}
	else{
		lastBlendingState = true;
	}
	blendEnabled = true;
}


void Utility::disableBlending(){
	if (blendEnabled){
		glDisable(GL_BLEND);
		lastSFactor = currentSFactor;
		lastDFactor = currentDFactor;
	}
	lastBlendingState = blendEnabled;

	blendEnabled = false;

}


void Utility::resetLastBlendingState(){
	if (lastBlendingState){
		enableBlending(lastSFactor, lastDFactor);
	}
	else{
		disableBlending();
	}
}


void Utility::switchFrontFaceMode(GLenum _Mode){
	if (frontFaceMode != _Mode){
		glFrontFace(_Mode);
		frontFaceMode = _Mode;
	}

}

/**********************************************************
bindTextures():
Bind all necessary textures for the mesh.
Gets texture ids from the Texture container.
send texture types of the mesh to the currently used shader.
***********************************************************/
void Utility::bindTextures(ShaderProgram& shader, const set<pair<GLuint, TextureType>>& textureFiles){
	int uniformTextureType = 0x0;
	if (textureFiles.size() > 0){
		for (pair<GLuint, TextureType>p : textureFiles){
			GLuint tIndex = (p.first);
			switch (p.second){
			case DIFFUSE_TEXTURE:
				glActiveTexture(GL_TEXTURE0);
				uniformTextureType = uniformTextureType | 0x1;
				break;
			case SPECULAR_TEXTURE:
				glActiveTexture(GL_TEXTURE2);
				uniformTextureType = uniformTextureType | 0x2;
				break;
			case ALPHA_TEXTURE:
				glActiveTexture(GL_TEXTURE3);
				uniformTextureType = uniformTextureType | 0x4;
				break;
			case NORMALS_TEXTURE:
				glActiveTexture(GL_TEXTURE4);
				uniformTextureType = uniformTextureType | 0x8;
				break;
			default:
				break;
			}
			glBindTexture(GL_TEXTURE_2D, tIndex);
		}
		shader.sendUniformData("textureType", uniformTextureType);

		//Binding Shadow Texture
		ShadowFrameBuffer::bindReadFB(1);
	}
}


void Utility::enableAnisotropicFiltering(){
	static bool query = false;

	static GLfloat largest_supported_anisotropy;
	if (!query){
		glGetFloatv(GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT, &largest_supported_anisotropy);
		query = true;
	}
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, largest_supported_anisotropy);
}


void Utility::toLowerCase(string& s){
	for (unsigned int i = 0; i < s.length(); i++){
		s[i] = tolower(s[i]);
	}
}