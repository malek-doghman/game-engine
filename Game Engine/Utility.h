#pragma once
#include "Common.h"
#include "ShaderProgram.h"


class Utility{
public:
	static void loadLightModels();
	static VIBuffer getLightModel(lightType);
	static ShaderProgram* getShader();
	static void disableFrameBuffer();

	static VIBuffer getRectangle();
	static VIBuffer getCube();
	static GLuint genEmptyTexture(unsigned int width, unsigned int height, unsigned int mipLevel, unsigned int pixelFormat);
	static GLuint genTextureFromData(unsigned char* rawData, unsigned int width, unsigned int height, unsigned int mipLevel, GLint channelCount);
	static void genVertexBuffer(GLuint*, unsigned int dataSize, void* data);

	static bool isRightHanded(const vec3& _Right, const vec3& _Up, const vec3& _Forward);

	static void enableBlending(GLenum sFactor, GLenum dFactor);
	static void disableBlending();
	static void resetLastBlendingState();

	static void switchFrontFaceMode(GLenum);
	static void bindTextures(ShaderProgram& shader, const set<pair<GLuint, TextureType>>& textureFiles);

	static void enableAnisotropicFiltering();
	static void toLowerCase(string& s);

	static void playSound( string fileName,bool repeat=false);

private:
	static void InitializeShader();
	static void loadModel(lightType, string fileName);
	static map<lightType, VIBuffer> lightModels;
	static bool loaded;
	static GLuint VAO;
	static VIBuffer rectangle;
	static VIBuffer cube;
	static ShaderProgram* lightShader;

	static void InitializeRectangle();
	static void InitializeCube();



	static bool blendEnabled, lastBlendingState;
	static GLenum lastSFactor, lastDFactor;
	static GLenum currentSFactor, currentDFactor;



	static GLenum frontFaceMode;

};