#include "WindowGL.h"
#include "GameComponent.h"
#include "Camera.h"
#include "FrameBuffer.h"
#include "ShaderAmbient.h"
#include "ShaderDiffuseSpecular.h"
#include "ShaderProgram.h"
#include "Model.h"
#include "LightShadow.h"
#include "Scene.h"
#include "Utility.h"
#include"CompleteFrameBuffer.h"
#include "Terrain.h"
#include "EcoSystem.h"
#include "CubeMap.h"
#include "SplashScreen.h"
#include "Textures.h"



int WindowGL::width = 800;
int WindowGL::height = 600;
float WindowGL::aspectRatio = 1.0f;
GLFWwindow* WindowGL::windowPointer = nullptr;
GLFWwindow* WindowGL::windowPointer2 = nullptr;
double WindowGL::elapsedTime = 0.0;
Terrain* WindowGL::t = nullptr;
vector<Light*> WindowGL::lights;
Camera* WindowGL::camera = nullptr;
FrameBuffer* WindowGL::frameBuffer;
vector<GameComponent*> WindowGL::gameComponents;
ShaderShadow* WindowGL::shadowShader = nullptr;
ShaderDiffuseSpecular* WindowGL::DSshader = nullptr;
ShaderAmbient* WindowGL::ambientShader = nullptr;
CubeMap* WindowGL::skybox = nullptr;

void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode)
{
	if (key == GLFW_KEY_ESCAPE || key == GLFW_KEY_F1 && action == GLFW_PRESS)
		glfwSetWindowShouldClose(window, GL_TRUE);
}


WindowGL::WindowGL(int width_, int height_, int red, int gre, int blue, string wTitle_)
{
	width = width_;
	height = height_;
	title = wTitle_;
	InitializeGLFW();
	InitializeGL();
	backgroundColor.push_back(static_cast<float>(red) / 255);
	backgroundColor.push_back(static_cast<float>(gre) / 255);
	backgroundColor.push_back(static_cast<float>(blue) / 255);

}

void WindowGL::InitializeGLFW(){
	if (glfwInit()){
		srand(clock());
		//windowPointer = glfwCreateWindow(width, height, &title[0],glfwGetPrimaryMonitor(), nullptr);
		windowPointer = glfwCreateWindow(width, height, &title[0], 0, nullptr);
		glfwSetInputMode(windowPointer, GLFW_CURSOR, GLFW_CURSOR_HIDDEN);
		glfwMakeContextCurrent(windowPointer);

		//Clear Screen with Black
		glClearColor(0, 0, 0, 0);
		glfwSwapBuffers(windowPointer);
		glfwPollEvents();

		Utility::playSound("data/sound/vacation.mp3");

	}
}



/**********************************************************
InitializeGL():
-Initializing the scene
-Initializing shaders
-Generating Initial lights
-Filling the gamecomponents container
***********************************************************/
void WindowGL::InitializeGL(){
	if (glewInit() == 0){

		//reset viewport size for default framebuffer
		glViewport(0, 0, width, height);

		glfwSetKeyCallback(windowPointer, key_callback);

		//Setting the Line Width for the bounding box
		glLineWidth(2.0f);

		//Setting Splash Screen
		thread* sp = new thread(loadDataRAM, this);

		SplashScreen::startSplashScreen();


	}
}


void WindowGL::loadDataRAM(WindowGL* window){
	glfwWindowHint(GLFW_VISIBLE, GL_FALSE);
	windowPointer2 = glfwCreateWindow(10, 10, "Test", nullptr, nullptr);
	glfwMakeContextCurrent(windowPointer2);

	if (glewInit() == 0){
		t = new Terrain("data/texture/terrain.png", 16000, 1000);
		EcoSystem* e = new EcoSystem(t);
		skybox = new CubeMap();

		e->addNewComponent("tree2.obj", 200, 150, PLANT);
		e->addNewComponent("test.obj", 100, 150, PLANT);
		e->addNewComponent("test03.obj", 100, 40, PLANT);
		e->addNewComponent("test04.obj", 100, 40, PLANT);
		e->addNewComponent("fl1.obj", 1000, 40, ROCK);
		e->addNewComponent("fl2.obj", 10000, 40, ROCK);
		e->addNewComponent("fl2.obj", 10000, 40, ROCK);
		
		
		camera = new Camera(*window, t);

		//Scene* scene = new Scene(vector < string > {}, vector < string > {});
		Scene* scene = new Scene(vector < string >(), vector < string >());

		gameComponents.push_back(scene);
		gameComponents.push_back(t);
		gameComponents.push_back(camera);

		gameComponents.push_back(e);

		SplashScreen::stopSplashScreen();
		glfwMakeContextCurrent(0);
	}
}

void WindowGL::loadShaders(){
	ambientShader = new ShaderAmbient(0.55f);
	DSshader = new ShaderDiffuseSpecular();
	shadowShader = new ShaderShadow();
}


void WindowGL::Render(){

	Utility::playSound("data/sound/wind.wav", true);

	skybox->Initialize();

	loadShaders();

	Textures::loadAllTexturesIntoVRAM();

	Light* light = new DirectionalLight();

	lights.push_back(light);

	frameBuffer = new CompleteFrameBuffer(width, height, "data/shader/motionBlur.frag", "data/shader/screen.vert");

	ShadowFrameBuffer::Initialize();

	InitializeElements();

	static float one = 1.0f;

	glEnable(GL_PROGRAM_POINT_SIZE);
	glEnable(GL_POINT_SPRITE_ARB);
	glEnable(GL_CULL_FACE);
	Utility::switchFrontFaceMode(GL_CCW);

	while (!glfwWindowShouldClose(windowPointer)){
		glViewport(0, 0, width, height);
		double iTime = glfwGetTime();

		glEnable(GL_DEPTH_TEST);

		glClearBufferfv(GL_COLOR, 0, &backgroundColor[0]);
		glClearBufferfv(GL_DEPTH, 0, &one);

		UpdateElements();
		DrawElements(*ambientShader);

		lightHandler();
		for (unsigned int i = 0; i < lights.size(); i++){
			Light* l = lights[i];
			if (l->getActiveStatus()){

				/********************************************
							GENERATING SHADOWS
				********************************************/

				generateShadows(l);

				/********************************************
							UPDATING FRAMEBUFFER
				********************************************/
				frameBuffer->UpdateFrameBuffer();
				glClearBufferfv(GL_COLOR, 0, &backgroundColor[0]);
				glClearBufferfv(GL_DEPTH, 0, &one);

				camera->resetLatestCameraWVP();
				DSshader->updateAttributes(l->getAttributes());

				DrawElements(*DSshader, l);
				skybox->Draw(*DSshader);

				/********************************************
					DRAWING THE CONTENT OF THE FRAMEBUFFER
						ON THE DEFAULT FRAMEBUFFER
				********************************************/

				frameBuffer->unBindFrameBuffer();

				Utility::switchFrontFaceMode(GL_CCW);

				Utility::enableBlending(GL_ONE, GL_ONE);

				glDisable(GL_DEPTH_TEST);

				frameBuffer->DrawFrameBuffer();


				Utility::disableBlending();

				glEnable(GL_DEPTH_TEST);
			}
		}

		/*glViewport(width, 0, width, height);
		ShadowFrameBuffer::Draw();*/
		glfwSwapBuffers(windowPointer);
		glfwPollEvents();
		elapsedTime = glfwGetTime() - iTime;

	}
}


/**********************************************************
InitializeElements():
Initialize all the Game Components found in
gameComponents vector
***********************************************************/
void WindowGL::InitializeElements(){
	for (GameComponent* component : gameComponents){
		component->Initialize();
	}
}


/**********************************************************
UpdateElements():
Updates only active Game Components found in
gameComponents vector
***********************************************************/
void WindowGL::UpdateElements(){
	for (GameComponent* component : gameComponents){
		if (component->getActiveStatus()){
			component->Update();
		}
	}

	//Update active lights in the scene
	for (Light* l : lights){
		if (l->getActiveStatus()){
			l->Update();
		}
	}
}

/**********************************************************
DrawElements():
Draws only active/drawable Game Components found in
gameComponents vector
***********************************************************/
void WindowGL::DrawElements(ShaderProgram& shader, Light* l){
	for (GameComponent* component : gameComponents){
		if (component->getActiveStatus() && component->getDrawStatus()){
			component->Draw(shader, l);
		}
	}

}

GLFWwindow* WindowGL::GetWindowPointer(){
	return(windowPointer);
}

float WindowGL::GetAspectRatio(){
	return(static_cast<float>(width) / height);
}

int WindowGL::GetHeight(){
	return(height);
}

int WindowGL::GetWidth(){
	return(width);
}

WindowGL::~WindowGL()
{
}



/**********************************************************
lightHandler():
Responsible of:
-Showing light objects
-Detect any light selection
-Adding lights
***********************************************************/
void WindowGL::lightHandler(){
	addLight();
	//showLights();
}

/**********************************************************
Draw all the lights objects when not in a shadow pass
this is to prevent light objects from being drawn
in the shadow map
***********************************************************/
void WindowGL::showLights(){

	if (!ShadowFrameBuffer::isShadowPass()){
		for (Light* l : lights){
			if ((l->getActiveStatus()) && (l->getDrawStatus())){
				l->Draw(*DSshader);
			}
		}
	}
}

void WindowGL::addLight(){
	static double t = glfwGetTime();
	if ((glfwGetKey(windowPointer, GLFW_KEY_ENTER)) && (t + 1 < glfwGetTime())){
		Light* light = new SpotLight();
		lights.push_back(light);
		t = glfwGetTime();
	}
}

/**********************************************************
Returns the last elapsed time between the last two frames
***********************************************************/
double WindowGL::getElapsedTime(){
	return(elapsedTime);
}


/**********************************************************
generateShadows():
Constructs a shadow map for a given light type
***********************************************************/
void WindowGL::generateShadows(Light* l){
	glm::vec4 white = vec4(10.0);
	static float one = 100.0f;
	Utility::switchFrontFaceMode(GL_CW);
	Utility::disableBlending();
	ShadowFrameBuffer::bindWriteFB();
	glClearBufferfv(GL_COLOR, 0, &white[0]);
	glClearBufferfv(GL_DEPTH, 0, &white[0]);
	if (l->getAttributes().type == DIRECTIONAL_LIGHT){
		for (int i = 0; i < 2; i++){
			for (int j = 0; j < 2; j++)
			{
				ShadowFrameBuffer::bindWriteFB(j, i);
				camera->setWVPMatrix(l);
				DrawElements(*shadowShader);

			}
		}
	}
	else{
		camera->setWVPMatrix(l);
		DrawElements(*shadowShader);
	}

	Utility::switchFrontFaceMode(GL_CCW);
	ShadowFrameBuffer::applyFilter();
	ShadowFrameBuffer::unbindWriteFB();
}