#pragma once
#include "Common.h"
#include "ShaderProgram.h"


class GameComponent;
class ShaderAmbient;
class ShaderDiffuseSpecular;
class Camera;
class Light;
class ShaderShadow;
class FrameBuffer;
class Terrain;
class CubeMap;

class WindowGL
{
public:
	WindowGL(int width_, int height_, int red = 0, int gre = 0, int blue = 0, string wTitle = "OpenGL APP");
	~WindowGL();

	static GLFWwindow* GetWindowPointer();
	void Render();
	static int GetHeight();
	static int GetWidth();
	static float GetAspectRatio();
	static double getElapsedTime();

private:

	vector<float>backgroundColor;
	string title;
	static int height, width;
	static GLFWwindow* windowPointer;
	static GLFWwindow* windowPointer2;
	static float aspectRatio;
	static vector<GameComponent*> gameComponents;
	static ShaderAmbient* ambientShader;
	static ShaderDiffuseSpecular* DSshader;
	static ShaderShadow* shadowShader;
	static FrameBuffer* frameBuffer;
	static CubeMap* skybox;
	static Camera* camera;
	static vector<Light*> lights;
	static double elapsedTime;
	static Terrain* t;


	void InitializeGLFW();
	void InitializeGL();
	void InitializeElements();
	void UpdateElements();
	void DrawElements(ShaderProgram& shader, Light* l = nullptr);
	void updateActiveLights();
	void lightHandler();
	void addLight();
	void showLights();
	void generateShadows(Light* l);
	static void loadDataRAM(WindowGL* window);
	void loadShaders();

};