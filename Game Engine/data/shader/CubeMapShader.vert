#version 330 core
layout (location = 0) in vec3 position;
out vec3 TexCoords;
uniform mat4 projection;
uniform mat4 view;

void main()
{
	mat4 viewNT = mat4(mat3(view));
    vec4 pos =   projection * viewNT * vec4(position, 1.0);  
	gl_Position = pos.xyww ; 				 
    TexCoords = position;
}  