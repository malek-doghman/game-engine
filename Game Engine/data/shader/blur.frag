#version 440

out vec4 color;

in vec2 textureCoords;

layout (binding = 1) uniform sampler2D sampler;

//TEXTURE RESOLUTION
uniform vec2 textureResolution;

void main(){
const float xt=1.0f/1280;
const float yt=1.0f/800;
const float detail=1.0f;

vec2 tCoords = (textureCoords +vec2(1.0))/2;


vec4 color0 = texture(sampler,vec2(tCoords.x,tCoords.y));
color0 += texture(sampler,vec2(tCoords.x+xt,tCoords.y));
color0 += texture(sampler,vec2(tCoords.x-xt,tCoords.y));
color0 += texture(sampler,vec2(tCoords.x,tCoords.y-yt));
color0 += texture(sampler,vec2(tCoords.x,tCoords.y+yt));
color0 += texture(sampler,vec2(tCoords.x+xt,tCoords.y-yt));
color0 += texture(sampler,vec2(tCoords.x+xt,tCoords.y+yt));
color0 += texture(sampler,vec2(tCoords.x-xt,tCoords.y-yt));
color0 += texture(sampler,vec2(tCoords.x-xt,tCoords.y+yt));

color = vec4(color0 /9);

}