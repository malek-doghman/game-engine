#version 440
layout(early_fragment_tests) in;
out vec4 color;

in vec2 textureCoords;

layout (binding = 1) uniform sampler2D sampler;

//TEXEL SIZE
//uniform vec2 texelSize;


void main(){
vec2 tCoords = (textureCoords +vec2(1.0))/2;

float depth=texture(sampler,vec2(tCoords.x,tCoords.y)).z;

if(depth>=1.00f){
discard;
}
else{
vec2 texelSize=vec2(1.0f/1800,1.0f/1000);
vec4 colorT = texture(sampler,vec2(tCoords.x,tCoords.y));

colorT += vec4(texture(sampler,vec2(tCoords.x - texelSize.x,tCoords.y)).rg,0.0,0.0);
colorT += vec4(texture(sampler,vec2(tCoords.x + texelSize.x,tCoords.y)).rg,0.0,0.0);

colorT += vec4(texture(sampler,vec2(tCoords.x - 2 * texelSize.x,tCoords.y)).rg,0.0,0.0);
colorT += vec4(texture(sampler,vec2(tCoords.x + 2 * texelSize.x,tCoords.y)).rg,0.0,0.0);

colorT += vec4(texture(sampler,vec2(tCoords.x - 3 * texelSize.x,tCoords.y)).rg,0.0,0.0);
colorT += vec4(texture(sampler,vec2(tCoords.x + 3 * texelSize.x,tCoords.y)).rg,0.0,0.0);

colorT.rg = colorT.rg/7;

color = colorT;
}
}