#version 420

const int DIRECTIONAL_LIGHT = 0;
const int POINT_LIGHT = 1;
const int SPOT_LIGHT = 2;

const float RADIAN_PER_DEGREE = 3.14 / 180;


in vec2 OUT_textureCoords;
in vec3 Normals;
in vec4 viewPosition;
in vec3 worldPosition;
uniform int textureType;	//Textures types that comes with the mesh

//Texture Type ENUMs
const int DIFFUSE_TEXTURE = 0x1;
const int SPECULAR_TEXTURE = 0x2;
const int ALPHA_TEXTURE = 0x4;
const int NORMALS_TEXTURE = 0x8;


//SHADOW CASCADE DEPTH
const float d1=200.0f;
const float d2=600.0f;
const float d3=1800.0f;
const float d4=4000.0f;

//SHADOW FILTER
const int NO_FILTER=0;
const int PCF_FILTER=1;
const int VSM_FILTER=2;




layout(binding=0) uniform sampler2D samplerRGB;			//Color Texture
layout(binding=1) uniform sampler2D samplerShadow;		//Shadow Texture
layout(binding=2) uniform sampler2D samplerSpecular;	//Specular Texture
layout(binding=3) uniform sampler2D samplerAlpha;		//Alpha Texture
layout(binding=4) uniform sampler2D samplerNormals;		//Normals Texture

uniform vec3 eyePos;			//Camera Position
uniform vec3 position;			//Light Position
uniform vec3 color;				//Light's Color
uniform vec3 diffuseColor;		//Diffuse Color
uniform vec3 direction;			//Light's Direction

uniform float shininess;		//Mesh' Shininess
uniform float intensity;		//Light Intensity
uniform float radius;			//Light Radius
uniform int lightType;			//Light Type



//LIGHT CASCADES MATRICES
uniform mat4 VPL1;
uniform mat4 VPL2;
uniform mat4 VPL3;
uniform mat4 VPL4;



//SHADOW MAP TEXEL SIZE
//uniform vec2 texelSize;
vec2 texelSize=vec2(1.0f/1800,1.0f/1000);

//MATRIX-NON CASCADED SHADOW
uniform mat4 VPMatrixLight;

uniform int instanced=0;

out vec4 Color;

vec3 getNormal();
float ToRadians(float degree);											//Convert angle from degree to radians
vec4 diffuseSpecularColor(vec4 sampledColor_,vec3 normalizedNormal_);	//Returns Fragment Color after lighting
float shadowIntensity(vec3 transformedFrag,float depthInShadowMap);		//Get Shadow Coefficient --NO FILTER
float PCFShadows(vec3 transformedFrag,vec2 tCoord,float detail);		//Get PCF (Percentage Close Filtering) Shadow Coefficient
float vsmShadows(float currentDepth,vec2 tCoord);						//Get VSM (Variance Shadow Mapping) Coefficient
vec4 sampleTexture(int TEXTURE_TYPE);									//Returns Texture
vec4 getCascadedShadow(int fliterType,float detail);					//Returns Shadow Coefficient
float getShadowCoefficient(vec3 transformedFrag,vec2 tCoord,int filterType,float detail);
float getShadowByLightType(int filtering,float detail);





void main(){

	vec4 sampledColor = sampleTexture(DIFFUSE_TEXTURE);
	if(sampledColor.a<0.3f){
		discard;
	}

	else{
	vec3 normalizedNormal = normalize(getNormal());
	//vec4 vv=getCascadedShadow(VSM_FILTER);
	float shadowI= getShadowByLightType(VSM_FILTER,3);
	//Color =   vec4(vec3(vv.w),1.0) * vec4(vv.xyz,1.0) ;//vec4(vec3(shadowI), 1.0);
	Color =   vec4(vec3(shadowI),1.0) * diffuseSpecularColor(sampledColor,normalizedNormal);//vec4(vec3(shadowI), 1.0);
	Color.a=gl_FragCoord.z;
	}

}


vec3 getNormal(){
	vec3 normal= (Normals);
	if((textureType & NORMALS_TEXTURE)>0){
		normal = vec3(texture(samplerNormals,OUT_textureCoords).xyz);
	}
	return(normal);
}


float ToRadians(float degree){
	return(RADIAN_PER_DEGREE * degree);
}



vec4 diffuseSpecularColor(vec4 sampledColor_,vec3 normalizedNormal_){
	vec4 tmp = sampledColor_;
	vec3 normal = normalizedNormal_;
	float roughness = 1 / shininess;

	if (lightType == DIRECTIONAL_LIGHT){
		vec3 lightDirection = -normalize(direction);
		float diffuseCoeff = clamp(dot(normalize(lightDirection),normal),0.0,1.0);
		vec4 diffuse = vec4(diffuseColor,1.0) * tmp * vec4(color * vec3(diffuseCoeff) * vec3(intensity), 1.0);

		vec3 reflectedLight = - normalize(reflect(lightDirection,normal));
		vec3 vertexCameraVector = normalize(eyePos - worldPosition.xyz);
		float specularCoeff = pow(clamp(dot(reflectedLight,vertexCameraVector),0.0,1.0),roughness);
		vec4 specular = vec4(vec3(diffuseCoeff) * vec3(specularCoeff),0.0);

		return(diffuse );

	}
	
	else if((lightType == POINT_LIGHT)||(lightType == SPOT_LIGHT)){
		vec3 lightDirection = position - worldPosition.xyz;
		float diffuseCoeff = clamp(dot(normalize(lightDirection),normal),0.0,1.0);
		float attenuation = pow(clamp(radius / length(lightDirection),0.0,1.0),1.5);
		vec4 diffuse =vec4(diffuseColor,1.0) * tmp * vec4(color * vec3(diffuseCoeff) * vec3(attenuation) * vec3(intensity), 1.0);
		
		vec3 reflectedLight = - normalize(reflect(lightDirection,normal));
		vec3 vertexCameraVector = normalize(eyePos - worldPosition.xyz);
		float specularCoeff = pow(clamp(dot(reflectedLight,vertexCameraVector),0.0,1.0),roughness);
		vec4 specular = vec4(vec3(attenuation) * vec3(diffuseCoeff) * vec3(specularCoeff),0.0);
		
		
		if(lightType == SPOT_LIGHT){
			float falloff = 0.0f;
			float c = (dot( -normalize(direction), normalize(lightDirection)));
			if(clamp(c,0.0,1.0)>0){
				float angl1 = cos(ToRadians(30.0f));
				float angl2 = cos(ToRadians(20.0f));
				falloff = smoothstep(angl1,angl2,c);
			}
			specular = specular * vec4(falloff);
			diffuse = diffuse * vec4(falloff);
		}
		
		return(diffuse);
		}


}

float shadowIntensity(vec3 transformedFrag,float depthInShadowMap){
if(depthInShadowMap>1.0){
		return(1.0);
	}

if((transformedFrag.x>0)&&(transformedFrag.x<1)&&((transformedFrag.y>0)&&(transformedFrag.y<1))){
	if(transformedFrag.z>depthInShadowMap){
		return(0.0);
	}
	else{
		return(1.0);
	}
}

else{
	return(0);
}

}


vec4 sampleTexture(int TEXTURE_TYPE){
	if((textureType & TEXTURE_TYPE)>0){
		switch(TEXTURE_TYPE){
			case DIFFUSE_TEXTURE:
			vec4 sampledColor = vec4(texture(samplerRGB,OUT_textureCoords).xyz,1.0);
			
			if((textureType & ALPHA_TEXTURE)>0){
				sampledColor.a = texture(samplerAlpha,OUT_textureCoords).x;
			}
			return(sampledColor);

			case SPECULAR_TEXTURE:
				return(vec4(texture(samplerSpecular,OUT_textureCoords).x));

			default:
				break;
		}
	}
	else
	{
		return(vec4(0.0));
	}
}

vec4 getCascadedShadow(int fliterType,float detail){
	const float pcfDetail=3.0f;
	float dist = abs(viewPosition.z / viewPosition.w);
	float depth=10000.0f;
	vec4 lVerticeF;
	
	
	if(instanced==1)
	return vec4(1.0,1.0,1.0,1.0);
	
	if((dist<d1)&&(dist>0.0f)){
		lVerticeF = VPL1 * vec4(worldPosition,1.0);
		lVerticeF = ((lVerticeF / lVerticeF.w).xyzw + vec4(1.0))/2 ;
		float b=getShadowCoefficient(lVerticeF.xyz,vec2(lVerticeF.x/2,lVerticeF.y/2),fliterType,detail);
		return vec4(0.43,0.98,0.12,b);
	
	}
	else if (dist<d2){

		lVerticeF = VPL2 *  vec4(worldPosition,1.0);
		lVerticeF = ((lVerticeF / lVerticeF.w).xyzw + vec4(1.0))/2 ;
		float b=getShadowCoefficient(lVerticeF.xyz,vec2(lVerticeF.x/2+0.5,lVerticeF.y/2),fliterType,detail);
		return vec4(0.95,0.0,0.0,b);
	}
	else if (dist<d3){
		lVerticeF = VPL3 *  vec4(worldPosition,1.0);
		lVerticeF = ((lVerticeF / lVerticeF.w).xyzw + vec4(1.0))/2 ;
		float b=getShadowCoefficient(lVerticeF.xyz,vec2(lVerticeF.x/2,lVerticeF.y/2+0.5),fliterType,detail);
		return vec4(0.23,0.35,0.65,b);
	}
	else if (dist<d4){
		lVerticeF = VPL4 *  vec4(worldPosition,1.0);
		lVerticeF = ((lVerticeF / lVerticeF.w).xyzw + vec4(1.0))/2 ;
		float b=getShadowCoefficient(lVerticeF.xyz,vec2(lVerticeF.x/2+0.5,lVerticeF.y/2+0.5),fliterType,detail);
		return vec4(1.0,0.98,0.5,b);
	}
	return vec4(1.0,1.0,1.0,1.0);

	}


float PCFShadows(vec3 transformedFrag,vec2 tCoord,float detail){
float lit=0.0f;
	for(float y=-detail;y<=detail;y++){
		for(float x=-detail;x<=detail;x++){
			float depth= texture(samplerShadow,vec2(tCoord.x + texelSize.x*x,tCoord.y + texelSize.y*y)).z;
			if(shadowIntensity(transformedFrag,depth)==1){
			lit++;
			}
			}
		}
	
float a = 2 * detail +1;
return(lit/(a*a));
}


float vsmShadows(float currentDepth,vec2 tCoord){
	const float constant = 5.0f;
	vec2 m = texture(samplerShadow,vec2(tCoord.x,tCoord.y)).xy;
	


	float e_x2 = m.y;
	float ex_2 = m.x * m.x;
	float variance_2 = (e_x2 - ex_2);
	float term = constant * (currentDepth - m.x) * (currentDepth - m.x);
	float coef = variance_2 /(variance_2 + term);
	coef=clamp(coef,0.0,1.0);
	if(currentDepth<=m.x){
	coef=1.0f;
	}
	return(coef);
}


float getShadowCoefficient(vec3 transformedFrag,vec2 tCoord,int filterType,float detail){
	switch(filterType){
		case NO_FILTER:
			float depth= texture(samplerShadow,vec2(tCoord.x,tCoord.y)).z;
			return(shadowIntensity(transformedFrag,depth));

		case PCF_FILTER:
			return(PCFShadows(transformedFrag,vec2(tCoord.x,tCoord.y),detail));

		case VSM_FILTER:
			return(vsmShadows(transformedFrag.z,vec2(tCoord.x,tCoord.y)));
		
		default:
			return(1.0f);
	}
}

float getShadowByLightType(int filtering,float detail){
	if(lightType==DIRECTIONAL_LIGHT){
		return(getCascadedShadow(filtering,detail).w);
		}
	else{
		vec4 lVerticeF=VPMatrixLight * vec4(worldPosition,1.0);
		lVerticeF = (vec4(lVerticeF/lVerticeF.w) + vec4(1.0))/2;
		return(getShadowCoefficient(lVerticeF.xyz,vec2(lVerticeF.x,lVerticeF.y),filtering,detail));
		}
}

