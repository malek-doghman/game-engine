#version 440

layout (location =0) in vec3 vPosition;

uniform mat4 WVPMatrix;


void main(){

	gl_Position = WVPMatrix * vec4(vPosition, 1.0);

}