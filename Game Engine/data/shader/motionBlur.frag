#version 440

layout(binding=1)uniform sampler2D sampler1;

in vec2 textureCoords;

uniform mat4 WVPMatrix;
uniform mat4 invertedWVP;
uniform mat4 oldWVP;


out vec4 Color;

int blurAmount=9;
uniform int enableMotionBlur;

vec2 clampVectorMinus(vec2 v);
bool isValid(vec2);

const float xt=1.0f/1280;
const float yt=1.0f/800;
const float detail=1.0f;


void main(){
	vec2 clipCoordsPos=(textureCoords + vec2(1.0))/2;
	
	if(enableMotionBlur==1){
		float depth=texture(sampler1,clipCoordsPos).a;
		vec4 worldPos = invertedWVP * vec4(clipCoordsPos,depth,1.0);
		worldPos/=worldPos.w;
		vec4 oldPos = oldWVP * worldPos;
		oldPos/=oldPos.w;

		vec2 t = clampVectorMinus(oldPos.xy);
		clipCoordsPos = clampVectorMinus(clipCoordsPos.xy);
		vec2 dir=(t - clipCoordsPos)/11;
		
		float b=blurAmount;
		vec4 avg=vec4(0.0f);
		vec2 tTmp=clipCoordsPos + b*dir;
		while((b>1)&&(isValid(tTmp)==false)){
			blurAmount--;
			tTmp=clipCoordsPos + b*dir;
			b=blurAmount;
		}
		for(int i=0;i<blurAmount;i++){
			vec4 tmp = texture(sampler1,clipCoordsPos + i*dir);
			avg+=tmp;
		}
			
		avg/=b;
		Color=avg;
		}
		else
		{
			Color=texture(sampler1,clipCoordsPos);
		}


	vec2 tCoords = (textureCoords +vec2(1.0))/2;
	
	
	vec4 color0 = texture(sampler1,vec2(tCoords.x,tCoords.y));
	color0 += texture(sampler1,vec2(tCoords.x+xt,tCoords.y));
	color0 += texture(sampler1,vec2(tCoords.x-xt,tCoords.y));
	color0 += texture(sampler1,vec2(tCoords.x,tCoords.y-yt));
	color0 += texture(sampler1,vec2(tCoords.x,tCoords.y+yt));
	color0 += texture(sampler1,vec2(tCoords.x+xt,tCoords.y-yt));
	color0 += texture(sampler1,vec2(tCoords.x+xt,tCoords.y+yt));
	color0 += texture(sampler1,vec2(tCoords.x-xt,tCoords.y-yt));
	color0 += texture(sampler1,vec2(tCoords.x-xt,tCoords.y+yt));
	
	Color = (Color + vec4(color0 /9))/2;

}


vec2 clampVectorMinus(vec2 v){
vec2 p;
p.x=clamp(v.x,-1.0,1.0);
p.y=clamp(v.y,-1.0,1.0);
return(p);
}


bool isValid(vec2 v){
return(!((v.x<0.0)||(v.x>1.0)||(v.y<0.0)||(v.y>1.0)));

}