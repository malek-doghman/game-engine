#version 440

layout(location=0) in vec3 vPosition;
layout(location=1) in vec2 textureCoords;
layout(location=2) in vec3 normals;
layout(location=3) in vec3 instancePosition;
layout(location=4) in vec2 instanceAngles;

uniform mat4 WVPMatrix;
uniform mat4 ViewMatrix;
uniform mat4 WorldMatrix;
uniform mat3 NormalMatrix;
uniform int instanced;

out vec2 OUT_textureCoords;
out vec3 Normals;
out vec3 worldPosition;
out vec4 viewPosition;


mat4 translateInstanceMatrix();
mat4 rotateMatrix(float xR,float yR,float zR);

void main(){

	mat4 translateInstance =translateInstanceMatrix();
	mat4 rotMatrix=rotateMatrix(instanceAngles.y,instanceAngles.x,0.0);
	
	vec4 wv = (WorldMatrix * vec4(vPosition,1.0));
	worldPosition=(wv/wv.w).xyz;
	viewPosition = ViewMatrix * vec4(worldPosition,1.0);
	Normals = NormalMatrix * normals;

	//Flipping texture coordiantes in Y axis because openGL reads texture from a lower-left origin while data loaded by soil has an origing in upper-left
	OUT_textureCoords = vec2(textureCoords.x,1.0-textureCoords.y);
	gl_Position = (WVPMatrix )* translateInstance * rotMatrix * vec4(vPosition,1.0);
}


mat4 translateInstanceMatrix(){
	mat4 translate=mat4(1.0);
	translate[3][0]=instancePosition.x;
	translate[3][1]=instancePosition.y;
	translate[3][2]=instancePosition.z;
	translate[3][3]=1.0f;
	return(translate);
}

mat4 rotateMatrix(float xR,float yR,float zR){
	mat4 rotate=mat4(
	cos(yR)*cos(zR),	cos(xR)*sin(zR)+sin(xR)*sin(yR)*cos(zR),	sin(xR)*sin(zR)-cos(xR)*sin(yR)*cos(zR),	0.0,
	-cos(yR)*sin(zR),	cos(xR)*cos(zR)-sin(xR)*sin(yR)*sin(zR),	sin(xR)*cos(zR)+cos(xR)*sin(yR)*sin(zR),	0.0,
	sin(yR),			-sin(xR)*cos(yR),							cos(xR)*cos(yR),							0.0,
	0.0,				0.0,										0.0,										1.0);
	
	return(rotate);
}