#version 440

out vec4 color;

in vec2 textureCoords;

layout (binding=1) uniform sampler2D sampler;

//TEXTURE RESOLUTION
uniform vec2 textureResolution;

void main(){

	vec2 tCoords = (textureCoords +vec2(1.0))/2;
	
	vec4 color0 = texture(sampler,vec2(tCoords.x,tCoords.y));
	
	color = vec4(color0.xyz,1.0)  ;


}