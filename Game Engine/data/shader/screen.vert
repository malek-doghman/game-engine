#version 440

layout(location = 0) in vec2 vPosition;

out vec2 textureCoords;


void main(){

	gl_Position = vec4(vPosition.x,vPosition.y,0.0,1.0);
	
	textureCoords = vPosition;

}