#version 440

uniform int textureType;

layout(binding=0) uniform sampler2D samplerRGB;
layout(binding=1) uniform sampler2D samplerShadow;
layout(binding=2) uniform sampler2D samplerSpecular;
layout(binding=3) uniform sampler2D samplerAlpha;
in vec2 OUT_textureCoords;

const int DIFFUSE_TEXTURE = 0x1;
const int SPECULAR_TEXTURE = 0x2;
const int SHADOW_TEXTURE = 0x3;
const int ALPHA_TEXTURE = 0x4;



uniform vec3 ambientColor;

uniform float intensity;

out vec3 Color;

vec4 sampleTexture(int TEXTURE_TYPE);

void main(){

	vec4 ColorTmp = sampleTexture(DIFFUSE_TEXTURE);
	
	if(ColorTmp.a<0.5){
		discard;
	}
	else{
		float z=gl_FragCoord.z+0.001;
		gl_FragDepth=z;
		Color = vec3(z,z*z,z);
	}

}

vec4 sampleTexture(int TEXTURE_TYPE){
	if(TEXTURE_TYPE == SHADOW_TEXTURE){
		



	}

	if((textureType & TEXTURE_TYPE)>0){
		switch(TEXTURE_TYPE){
			case DIFFUSE_TEXTURE:
			vec4 sampledColor = vec4(texture(samplerRGB,OUT_textureCoords).xyz,1.0);
			if((textureType & ALPHA_TEXTURE)>0){
				sampledColor.a = texture(samplerAlpha,OUT_textureCoords).x;
			}
			return(sampledColor);

			case SPECULAR_TEXTURE:
				return(vec4(texture(samplerSpecular,OUT_textureCoords).x));

			default:
				break;
		}
	}
	else
	{
		return(vec4(0.0));
	}
}