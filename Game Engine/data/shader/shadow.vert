#version 440

layout(location=0) in vec3 vPosition;
layout(location=1) in vec2 textureCoords;
layout(location=3) in vec3 instancePosition;

uniform mat4 WVPMatrix;
uniform mat4 WVPMatrixLight;
uniform mat4 ViewMatrix;

out vec2 OUT_textureCoords;

uniform int instanced=0;

mat4 translateInstanceMatrix();

void main(){
	vec4 wvp;

	if(instanced==1){

		mat4 translate = translateInstanceMatrix();
		wvp = WVPMatrix * translate * vec4(vPosition,1.0);

	}
	else{

		wvp = WVPMatrix * vec4(vPosition,1.0);

	}

	OUT_textureCoords = textureCoords;
	gl_Position = wvp;
}


mat4 translateInstanceMatrix(){
	mat4 translate=mat4(1.0);
	translate[3][0]=instancePosition.x;
	translate[3][1]=instancePosition.y;
	translate[3][2]=instancePosition.z;
	translate[3][3]=1.0f;

	return(translate);
}