#version 440

out vec4 color;

in vec2 textureCoords;

uniform float time;

layout (binding=1) uniform sampler2D sampler;

//TEXTURE RESOLUTION
uniform vec2 textureResolution;

void main(){

	vec2 uv = (textureCoords -vec2(0.7))/2;
	vec2 uv2 = (textureCoords +vec2(1.0))/2;
	
	float t=abs(time);
	


	/*********************** THIS PART IS STOLEN ******************************/

	vec3 finalColor = vec3 ( 1.0, 0.7, 0.0 );
		
	finalColor *= abs( 1.0 / (sin( uv.x + sin(uv.y+time)* 0.10 ) * 40.0) );

	/*************************************************************************/
	
	vec4 color0 = texture(sampler,uv2);
	
	if(color0.r>0.9){
		color=color0;
	}
	else{
		color=color0+vec4(finalColor,1.0);
	}

}

