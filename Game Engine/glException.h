#pragma once
#include<stdexcept>
#include "Common.h"
using std::runtime_error;

class glException :public runtime_error
{
public:
	glException(string errorMsg);
	~glException();
};

