#include "WindowGL.h"
#include "glException.h"

#define GLEW_MX

void main(){


	WindowGL window(1280, 800, 0, 0, 0, "Game Engine");
	try{
		window.Render();
	}

	catch (glException& ex){
		std::cout << ex.what();
	}

}