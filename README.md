# Game Engine #

### End Of Year Project 2 ###
* 3D Forest scene (c++ / OpenGL 4.) that shows blur , motion blur , shadows  
* Game Engine 2.0

### How do I get set up? ###

* Download and compile under Visual Studio 2017
* Dependencies : 

           OpenGL library         | GLEW
           --------------------------------- 
           Window Library         | GLFW 3
           ---------------------------------
           Texture importer       | Soil 
           ---------------------------------
           Math Library           | Glm 
           ---------------------------------
           Object importer        | assimp

* Library included no need to install or downlaod anything
* You will also need an OpenGL 4.4 capable GPU